package dst.ass1.nosql.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.Mongo;
import dst.ass1.nosql.IMongoDbQuery;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manuel Faux
 */
public class MongoDbQuery implements IMongoDbQuery {

    private DB db;

    public MongoDbQuery() {
        try {
            Mongo mongo = new Mongo();
            db = mongo.getDB("dst");
        } catch (UnknownHostException e) {
        }
    }

    @Override
    public Long findLastUpdatedByJobId(Long jobId) {
        DBCollection jobResults = db.getCollection("JobResult");
        BasicDBObject query = new BasicDBObject("job_id", jobId);
        DBObject result = jobResults.findOne(query, new BasicDBObject("last_updated", 1));
        if (result == null) {
            return null;
        }

        return (Long) result.get("last_updated");
    }

    @Override
    public List<Long> findLastUpdatedGt(Long time) {
        DBCollection jobResults = db.getCollection("JobResult");

        BasicDBObject query = new BasicDBObject("last_updated", new BasicDBObject("$gt", time));
        DBCursor results = jobResults.find(query, new BasicDBObject("job_id", 1));
        System.out.println("Search for jobs ended @'" + time + "' returned " + results.size() + " results.");

        ArrayList<Long> result = new ArrayList<Long>();
        try {
            while (results.hasNext()) {
                DBObject doc = results.next();
                System.out.println("Result: " + doc.toString());
                result.add((Long) doc.get("job_id"));
            }
        } finally {
            results.close();
        }

        System.out.println("IDs for job search: " + result);
        return result;
    }

    @Override
    public List<DBObject> mapReduceWorkflow() {
        DBCollection jobResults = db.getCollection("JobResult");

        String map = "function () {"
                + "    for(var key in this)"
                + "        if(key != '_id' && key != 'job_id' && key != 'last_updated')"
                + "            emit(key, 1);"
                + "};";
        String reduce = "function(key , values){"
                + "    var total = 0;"
                + "    values.forEach(function(cnt) {total += cnt;});"
                + "    return total;"
                + "};";

        MapReduceOutput results = jobResults.mapReduce(map, reduce, null, MapReduceCommand.OutputType.INLINE, null);

        ArrayList<DBObject> result = new ArrayList<DBObject>();
        for (DBObject doc : results.results()) {
            System.out.println(doc);
            result.add(doc);
        }

        return result;
    }
}
