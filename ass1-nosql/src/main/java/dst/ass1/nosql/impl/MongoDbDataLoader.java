package dst.ass1.nosql.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;
import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.IJob;
import dst.ass1.nosql.IMongoDbDataLoader;
import dst.ass1.nosql.MongoTestData;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;

/**
 *
 * @author Manuel Faux
 */
public class MongoDbDataLoader implements IMongoDbDataLoader {

    private DAOFactory daoFactory;
    
    private DB db;
    
    public MongoDbDataLoader(EntityManager em) {
        daoFactory = new DAOFactory((Session) em.getDelegate());
     
        try {
            Mongo mongo = new Mongo();
            db = mongo.getDB("dst");
        }
        catch (UnknownHostException e) {}
    }
    
    @Override
    public void loadData() throws Exception {
        DBCollection jobResults = db.getCollection("JobResult");
        MongoTestData testData = new MongoTestData();
        
        List<IJob> jobs = daoFactory.getJobDAO().findJobForStatusFinishedStartandFinish(null, null);
        for (Iterator<IJob> it = jobs.iterator(); it.hasNext();) {
            IJob job = it.next();
            jobResults.insert(new BasicDBObject("job_id", job.getId())
                    .append("last_updated", job.getExecution().getEnd().getTime())
                    .append(testData.getDataDesc(job.getId().intValue()), JSON.parse(testData.getStringData(job.getId().intValue()))));
        }
        
        jobResults.createIndex(new BasicDBObject("job_id", 1));
    }
    
}
