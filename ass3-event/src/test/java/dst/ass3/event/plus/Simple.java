/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dst.ass3.event.plus;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.StatementAwareUpdateListener;
import dst.ass3.dto.TaskDTO;
import dst.ass3.event.Constants;
import dst.ass3.event.EventingFactory;
import dst.ass3.event.EventingFactory;
import dst.ass3.event.IEventProcessing;
import dst.ass3.event.IEventProcessing;
import dst.ass3.model.ITask;
import dst.ass3.model.TaskComplexity;
import dst.ass3.model.TaskStatus;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel
 */
public class Simple {

    public Simple() {
    }

    @BeforeClass
    public static void setUpClass() {
        BasicConfigurator.configure();
    }

    @Test
    public void test1() throws InterruptedException {
        IEventProcessing ep = EventingFactory.getInstance();
        ep.initializeAll(new StatementAwareUpdateListener() {
            @Override
            public void update(EventBean[] newEvents, EventBean[] oldEvents,
                    EPStatement s, EPServiceProvider p) {
                //log.log(Level.INFO, "Recieved {0} new events and {1} old events.", new Object[]{ebs.length, ebs1.length});
                System.out.print("----------------------------\n");

                if (newEvents == null) {
                    System.out.println("new null");
                } else {
                    String type = newEvents[0].getEventType().getName();
                    System.out.println("EventType: " + type);
                    
                    if (type.equals(Constants.EVENT_TASK_DURATION))
                        System.out.println("Duration: " + newEvents[0].get("duration"));
                    else
                        System.out.println("Timestamp: " + newEvents[0].get("timestamp"));
                }
                if (oldEvents == null) {
                    System.out.println("old null");
                } else {
                    System.out.printf("Old: %s\n", oldEvents.length);
                }
            }
        }, false);

        ITask task = EventingFactory.createTask(1L, 1L, TaskStatus.ASSIGNED, "", TaskComplexity.CHALLENGING);
        ep.addEvent(task);
        task.setStatus(TaskStatus.READY_FOR_PROCESSING);
        ep.addEvent(task);
        Thread.sleep(1000);
        task.setStatus(TaskStatus.PROCESSED);
        ep.addEvent(task);
    }
}