package dst.ass3.event.impl;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.StatementAwareUpdateListener;
import dst.ass3.dto.TaskDTO;
import dst.ass3.event.Constants;
import dst.ass3.event.IEventProcessing;
import dst.ass3.model.ITask;
import dst.ass3.model.TaskStatus;

/**
 *
 * @author Manuel
 */
public class EventProcessor implements IEventProcessing {

    private EPServiceProvider epService;
    
    @Override
    public void initializeAll(StatementAwareUpdateListener listener, boolean debug) {
        Configuration config = new Configuration();
        config.addEventType(TaskDTO.class);
        config.addImport(TaskStatus.class);
        
        epService = EPServiceProviderManager.getDefaultProvider(config);
        
        EPAdministrator admin = epService.getEPAdministrator();
        
        admin.createEPL(String.format("create schema %s(jobId long, timestamp long)", Constants.EVENT_TASK_ASSIGNED));
        admin.createEPL(String.format("create schema %s(jobId long, duration long)", Constants.EVENT_TASK_DURATION));
        admin.createEPL(String.format("create schema %s(jobId long, timestamp long)", Constants.EVENT_TASK_PROCESSED));
        
        
        admin.createEPL(String.format("insert into %s(jobId, timestamp) "
                + "select jobId, current_timestamp as timestamp "
                + "from TaskDTO(status=TaskStatus.ASSIGNED)", Constants.EVENT_TASK_ASSIGNED));
        admin.createEPL(String.format("insert into %s(jobId, timestamp) "
                + "select jobId, current_timestamp as timestamp "
                + "from TaskDTO(status=TaskStatus.PROCESSED)", Constants.EVENT_TASK_PROCESSED));
        
        admin.createEPL(String.format("insert into %s(jobId, duration) "
                + "select b.jobId, b.timestamp-a.timestamp "
                + "from pattern [every a=%s -> b=%s(jobId = a.jobId)]",
                Constants.EVENT_TASK_DURATION, Constants.EVENT_TASK_ASSIGNED, Constants.EVENT_TASK_PROCESSED));
        
        //admin.compileEPL("on TaskDTO(status=TaskStatus.ASSIGNED) "
        //        + "insert into " + Constants.EVENT_TASK_ASSIGNED + " select *");
        //admin.createEPL("insert into " + Constants.EVENT_TASK_ASSIGNED + " select b.jobId as jobId from pattern [every (a=TaskDTO(status = TaskStatus.ASSIGNED) -> b=TaskDTO(status = TaskStatus.READY_FOR_PROCESSING and jobId = a.jobId))]");
        //EPStatement statement = admin.createEPL("select * from " + Constants.EVENT_TASK_ASSIGNED);
        admin.createEPL("select * from " + Constants.EVENT_TASK_DURATION).addListener(listener);
        admin.createEPL("select * from " + Constants.EVENT_TASK_PROCESSED).addListener(listener);
        admin.createEPL("select * from " + Constants.EVENT_TASK_ASSIGNED).addListener(listener);
        
        admin.createEPL(String.format("select avg(duration) as %s "
                + "from %s.win:time(15 sec)",
                Constants.EVENT_AVG_TASK_DURATION, Constants.EVENT_TASK_DURATION))
                .addListener(listener);
        
        admin.createEPL("select a "
                + "from pattern [every a=TaskDTO(status=dst.ass3.model.TaskStatus.PROCESSING_NOT_POSSIBLE)"
                + "-> b=TaskDTO(a.jobId = jobId and status=dst.ass3.model.TaskStatus.READY_FOR_PROCESSING)]"
                + "group by a.jobId "
                + "having count(*) >= 3 ")
                .addListener(listener);
    }

    @Override
    public void addEvent(ITask task) {
        epService.getEPRuntime().sendEvent(task);
    }

    @Override
    public void close() {
        epService.destroy();
    }
    
}
