/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dst.ass3.jms.plus;

import dst.ass3.jms.JMSFactory;
import dst.ass3.jms.scheduler.IScheduler;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel
 */
public class JNDITest {

    public JNDITest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createTask() {
        IScheduler s = JMSFactory.createScheduler();
        s.start();
        s.assign(3);
    }
}