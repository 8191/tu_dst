/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dst.ass3.jms.cluster.impl;

import dst.ass3.dto.RateTaskDTO;
import dst.ass3.jms.cluster.ICluster;
import dst.ass3.jms.cluster.ICluster.IClusterListener.TaskDecideResponse;
import dst.ass3.model.TaskStatus;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Manuel
 */
public class Cluster implements ICluster, MessageListener {
    
    private static final Logger log = LoggerFactory.getLogger(Cluster.class);
    
    private final String clusterName;
    
    private static ConnectionFactory connectionFactory;
    
    private static Queue clusterQueue;
    
    private static Queue serverQueue;
    
    private Connection connection;
    
    private MessageConsumer consumer;
    
    private IClusterListener listener;
    
    static {
        try {
            InitialContext ctx = new InitialContext();
            connectionFactory = (ConnectionFactory)ctx.lookup("dst/Factory");
            serverQueue = (Queue)ctx.lookup("queue/dst/ServerQueue");
            clusterQueue = (Queue)ctx.lookup("queue/dst/ClusterQueue");
        } catch (NamingException ex) {
            log.error(ex.getMessage());
        }
    }
    
    public Cluster(String name) {
        clusterName = name;
    }
    
    @Override
    public void start() {
        try {            
            connection = connectionFactory.createConnection();
            connection.setClientID(clusterName);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            consumer = session.createConsumer(clusterQueue);
            consumer.setMessageListener(this);
            connection.start();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void stop() {
        try {
            consumer.close();
            connection.close();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void setClusterListener(IClusterListener listener) {
        this.listener = listener;
    }

    @Override
    public void onMessage(Message msg) {
        try {
            if (msg instanceof ObjectMessage) {
                Object obj = ((ObjectMessage)msg).getObject();

                if (obj instanceof RateTaskDTO) {
                    log.info("Received RateTaskDTO: " + (RateTaskDTO)obj);
                    RateTaskDTO taskDTO = ((RateTaskDTO)obj);
                    processNewTask(taskDTO);
                }
            }
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void processNewTask(RateTaskDTO rateTaskDTO) {
        if (listener == null) {
            log.error("Listener not set.");
            return;
        }
        
        log.info("Processing task " + rateTaskDTO.getJobId());
        // Rate task
        TaskDecideResponse taskDecideResponse = listener.decideTask(rateTaskDTO, clusterName);
        log.info("Decicion response " + taskDecideResponse);
        
        rateTaskDTO.setRatedBy(clusterName);
        if (taskDecideResponse.resp == IClusterListener.TaskResponse.ACCEPT) {
            rateTaskDTO.setComplexity(taskDecideResponse.complexity);
            rateTaskDTO.setStatus(TaskStatus.READY_FOR_PROCESSING);
        }
        else {
            //startListening();
            rateTaskDTO.setStatus(TaskStatus.PROCESSING_NOT_POSSIBLE);
        }
            
        try {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.createProducer(serverQueue).send(session.createObjectMessage(rateTaskDTO));
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
}
