package dst.ass3.jms.scheduler.impl;

import dst.ass3.dto.InfoTaskDTO;
import dst.ass3.dto.NewTaskDTO;
import dst.ass3.dto.TaskDTO;
import dst.ass3.jms.scheduler.IScheduler;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Manuel Faux
 */
public class Scheduler implements IScheduler, MessageListener {
    
    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);
    
    private static ConnectionFactory connectionFactory;
    
    private static Queue schedulerQueue;
    
    private static Queue serverQueue;
    
    private Connection connection;
    
    private MessageConsumer consumer;
    
    private ISchedulerListener listener;

    static {
        try {            
            InitialContext ctx = new InitialContext();
            connectionFactory = (ConnectionFactory)ctx.lookup("dst/Factory");
            schedulerQueue = (Queue)ctx.lookup("queue/dst/SchedulerQueue");
            serverQueue = (Queue)ctx.lookup("queue/dst/ServerQueue");
        } catch (NamingException ex) {
            log.error(ex.getMessage());
        }
    }
    
    @Override
    public void start() {
        log.info("Starting scheduler...");
        try {            
            connection = connectionFactory.createConnection();
            connection.setClientID("scheduler");
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            consumer = session.createConsumer(schedulerQueue);
            consumer.setMessageListener(this);
            connection.start();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
        log.info("Scheduler started.");
    }

    @Override
    public void stop() {
        try {
            consumer.close();
            connection.close();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void assign(long jobId) {
        try {
            NewTaskDTO newTaskDTO = new NewTaskDTO(jobId);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.createProducer(serverQueue).send(session.createObjectMessage(newTaskDTO));
            log.info("Assigned task " + jobId);
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void info(long taskId) {
        try {
            InfoTaskDTO infoTaskDTO = new InfoTaskDTO(taskId);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.createProducer(serverQueue).send(session.createObjectMessage(infoTaskDTO));
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void setSchedulerListener(ISchedulerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onMessage(Message msg) {
        log.info("Received message " + msg.getClass().getCanonicalName());
        try {
            if (msg instanceof ObjectMessage) {
                Object obj = ((ObjectMessage)msg).getObject();

                if (obj instanceof TaskDTO) {
                    log.info("Received TaskDTO: " + (TaskDTO)obj);
                    if (listener == null) {
                        log.warn("Listener not set.");
                    }
                    
                    String msgType = msg.getStringProperty("type");
                    
                    if (msgType.equals("created")) {
                        // New task created
                        if (listener != null)
                            listener.notify(ISchedulerListener.InfoType.CREATED, (TaskDTO)obj);
                    }
                    else if (msgType.equals("info")) {
                        // Task info requested
                        if (listener != null)
                            listener.notify(ISchedulerListener.InfoType.INFO, (TaskDTO)obj);
                    }
                    else if (msgType.equals("denied")) {
                        if (listener != null)
                            listener.notify(ISchedulerListener.InfoType.DENIED, (TaskDTO)obj);
                    }
                    else if (msgType.equals("processed")) {
                        if (listener != null)
                            listener.notify(ISchedulerListener.InfoType.PROCESSED, (TaskDTO)obj);
                    }
                    else {
                        log.warn("Received unexpected message type '" + msgType + "'.");
                    }
                }
            }
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

}
