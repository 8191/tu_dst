package dst.ass3.jms.computer.impl;

import dst.ass3.dto.ProcessTaskDTO;
import dst.ass3.jms.computer.IComputer;
import dst.ass3.model.TaskComplexity;
import dst.ass3.model.TaskStatus;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Manuel
 */
public class Computer implements IComputer, MessageListener {

    private static final Logger log = LoggerFactory.getLogger(Computer.class);
    
    private final String name;
    
    private final String cluster;
    
    private final TaskComplexity complexity;
    
    private static ConnectionFactory connectionFactory;
    
    private static Topic computerTopic;
    
    private static Queue serverQueue;
    
    private Connection connection;
    
    private MessageConsumer consumer;
    
    private IComputerListener listener;
    
    public Computer(String name, String cluster, TaskComplexity complexity) {
        this.name = name;
        this.cluster = cluster;
        this.complexity = complexity;
    }
    
    static {
        try {        
            InitialContext ctx = new InitialContext();
            connectionFactory = (ConnectionFactory)ctx.lookup("dst/Factory");
            computerTopic = (Topic)ctx.lookup("topic/dst/ComputerTopic");
            serverQueue = (Queue)ctx.lookup("queue/dst/ServerQueue");
        } catch (NamingException ex) {
            log.error(ex.getMessage());
        }
    }
    
    @Override
    public void start() {
        log.info("Starting computer '" + name + "' in cluster '" + cluster + ".");
        try {
            connection = connectionFactory.createConnection();
            connection.setClientID(cluster + name);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            log.info("Complexity: " + complexity.ordinal());
            consumer = session.createDurableSubscriber(computerTopic, name, "cluster = '" + cluster + "' AND complexity = " + complexity.ordinal(), true);
            consumer.setMessageListener(this);
            connection.start();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void stop() {
        try {
            consumer.close();
            connection.close();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void setComputerListener(IComputerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onMessage(Message msg) {
        log.info("Received message " + msg.getClass().getCanonicalName());
        try {
            if (msg instanceof ObjectMessage) {
                Object obj = ((ObjectMessage)msg).getObject();

                if (obj instanceof ProcessTaskDTO) {
                    log.info("Received ProcessTaskDTO: " + (ProcessTaskDTO)obj);
                    ProcessTaskDTO processTaskDTO = ((ProcessTaskDTO)obj);
                    processTask(processTaskDTO);
                }
            }
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void processTask(ProcessTaskDTO processTaskDTO) {
        if (listener == null) {
            log.error("Listener not set.");
            return;
        }
        
        listener.waitTillProcessed(processTaskDTO, name, complexity, cluster);
        processTaskDTO.setStatus(TaskStatus.PROCESSED);
        
        // Inform scheduler of finished task
        try {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.createProducer(serverQueue).send(session.createObjectMessage(processTaskDTO));
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
}
