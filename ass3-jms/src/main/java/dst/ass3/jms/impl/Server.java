/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dst.ass3.jms.impl;

import dst.ass3.dto.InfoTaskDTO;
import dst.ass3.dto.NewTaskDTO;
import dst.ass3.dto.ProcessTaskDTO;
import dst.ass3.dto.RateTaskDTO;
import dst.ass3.dto.TaskDTO;
import dst.ass3.model.Task;
import dst.ass3.model.TaskComplexity;
import dst.ass3.model.TaskStatus;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.EntityManagerImpl;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Manuel
 */
@MessageDriven(mappedName = "queue/dst/ServerQueue")
public class Server implements MessageListener {
    
    private static org.slf4j.Logger log = LoggerFactory.getLogger(Server.class);
    
    @PersistenceContext
    protected EntityManager em;
    
    @Resource(lookup = "dst/Factory")
    private ConnectionFactory connectionFactory;
    
    @Resource(lookup = "queue/dst/SchedulerQueue")
    private Queue schedulerQueue;
    
    @Resource(lookup = "queue/dst/ClusterQueue")
    private Queue clusterQueue;
    
    @Resource(lookup = "topic/dst/ComputerTopic")
    private Topic computerTopic;
    
    private Connection connection;
    
    @PostConstruct
    public void postConstruct() {
        try {
            connection = connectionFactory.createConnection();
            em.getEntityManagerFactory();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
        
        log.info("Bean created");
    }
    
    @PreDestroy
    public void preDestroy() {
        try {
            connection.close();
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }

    @Override
    public void onMessage(Message msg) {
        log.info("Received message " + msg.getClass().getCanonicalName());
        try {
            if (msg instanceof ObjectMessage) {
                    Object obj = ((ObjectMessage)msg).getObject();
                    
                    // Sent by Scheduler#info() to request task info
                    if (obj instanceof InfoTaskDTO) {
                        log.info("Received InfoTaskDTO: " + (InfoTaskDTO)obj);
                        retrieveTaskInfo((InfoTaskDTO)obj);
                    }
                    // Sent my Scheduler#assign()
                    else if (obj instanceof NewTaskDTO) {
                        log.info("Received NewTaskDTO: " + (NewTaskDTO)obj);
                        processNewTask((NewTaskDTO)obj);
                    }
                    // Answer of cluster for task rating
                    else if (obj instanceof RateTaskDTO) {
                        log.info("Received RateTaskDTO: " + (RateTaskDTO)obj);
                        receiveTaskRating((RateTaskDTO)obj);
                    }
                    // Answer of computer for finished processing
                    else if (obj instanceof ProcessTaskDTO) {
                        log.info("Received ProcessTaskDTO: " + (ProcessTaskDTO)obj);
                        taskFinished((ProcessTaskDTO)obj);
                    }
                    else {
                        log.warn("Recieved unexpected message " + obj.getClass().getCanonicalName());
                    }
            }
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void retrieveTaskInfo(InfoTaskDTO infoTaskDTO) {
        try {
            Criteria criteria = ((EntityManagerImpl)em.getDelegate()).getSession().createCriteria(Task.class);
            criteria.add(Restrictions.eq("id", infoTaskDTO.getTaskId()));
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            // Answer scheduler with task info
            TaskDTO taskDTO = new TaskDTO((Task)criteria.uniqueResult());
            ObjectMessage message = session.createObjectMessage(taskDTO);
            message.setStringProperty("type", "info");
            session.createProducer(schedulerQueue).send(message);
            
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void processNewTask(NewTaskDTO newTaskDTO) {
        log.info("Process new task " + newTaskDTO);
        try {
            Task task = new Task(newTaskDTO.getJobId(), TaskStatus.ASSIGNED, null, TaskComplexity.UNRATED);
            log.info("Persisting task " + newTaskDTO);
            org.hibernate.Session dbSession = ((EntityManagerImpl)em.getDelegate()).getSession();
            dbSession.persist(task);
            dbSession.flush();
            log.info("Persisted new task " + newTaskDTO);
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            ObjectMessage schedMsg = session.createObjectMessage(new TaskDTO(task));
            schedMsg.setStringProperty("type", "created");
            // Answer scheduler that task was created
            session.createProducer(schedulerQueue).send(schedMsg);
            // Send task to cluster
            session.createProducer(clusterQueue).send(session.createObjectMessage(new RateTaskDTO(task)));
            
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void receiveTaskRating(RateTaskDTO rateTaskDTO) {
        org.hibernate.Session dbSession = ((EntityManagerImpl)em.getDelegate()).getSession();
        Transaction tx = dbSession.beginTransaction();
        Criteria criteria = dbSession.createCriteria(Task.class);
        criteria.add(Restrictions.eq("id", rateTaskDTO.getId()));
        Task task = (Task)criteria.uniqueResult();
        task.setComplexity(rateTaskDTO.getComplexity());
        task.setRatedBy(rateTaskDTO.getRatedBy());
        task.setStatus(rateTaskDTO.getStatus());
        tx.commit();
        log.info("Persisted task " + rateTaskDTO);
        
        try {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            if (rateTaskDTO.getStatus() == TaskStatus.READY_FOR_PROCESSING) {
                ObjectMessage message = session.createObjectMessage(new ProcessTaskDTO(task));
                message.setStringProperty("cluster", rateTaskDTO.getRatedBy());
                message.setIntProperty("complexity", rateTaskDTO.getComplexity().ordinal());
                session.createProducer(computerTopic).send(message);
            }
            else if (rateTaskDTO.getStatus() == TaskStatus.PROCESSING_NOT_POSSIBLE) {
                ObjectMessage message = session.createObjectMessage(new TaskDTO(task));
                message.setStringProperty("type", "denied");
                session.createProducer(schedulerQueue).send(message);
            }
            else {
                log.warn("Received unexpected task status: " + rateTaskDTO);
            }
        } catch (JMSException ex) {
            log.error(ex.getMessage());
        }
    }
    
    private void taskFinished(ProcessTaskDTO processTaskDTO) {
        if (processTaskDTO.getStatus() != TaskStatus.PROCESSED) {
            log.error("Task could not be processed: " + processTaskDTO);
        }
        
        org.hibernate.Session dbSession = ((EntityManagerImpl)em.getDelegate()).getSession();
        Transaction tx = dbSession.beginTransaction();
        Criteria criteria = dbSession.createCriteria(Task.class);
        criteria.add(Restrictions.eq("id", processTaskDTO.getId()));
        Task task = (Task)criteria.uniqueResult();
        
        if (task != null) {
            // Task not set as processed
            if (task.getStatus() != processTaskDTO.getStatus()) {
                task.setStatus(processTaskDTO.getStatus());
                tx.commit();
                log.info("Persisted task " + task);
                
                try {
                    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    ObjectMessage message = session.createObjectMessage(new TaskDTO(task));
                    message.setStringProperty("type", "processed");
                    session.createProducer(schedulerQueue).send(message);
                } catch (JMSException ex) {
                    log.error(ex.getMessage());
                }
                
                return;
            }
            else {
                log.info("Task was already marked as processed: " + task);
            }
        }
        
        tx.rollback();
        log.info("Did not persist task " + task);
    }
}
