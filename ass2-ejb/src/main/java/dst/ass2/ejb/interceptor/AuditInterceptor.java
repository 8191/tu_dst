package dst.ass2.ejb.interceptor;

import dst.ass2.ejb.model.impl.AuditLog;
import dst.ass2.ejb.model.impl.AuditParameter;
import java.util.Date;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.ejb.EntityManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditInterceptor {
    
    private static Logger logger = LoggerFactory.getLogger(AuditInterceptor.class);
    
    @PersistenceContext
    private EntityManager em;
    
    @AroundInvoke
    public Object jobManagementInterceptor(InvocationContext ctx) throws Exception {
        String method = ctx.getMethod() != null ? ctx.getMethod().getName() : "Unknown";
        Object[] parameters = ctx.getParameters();
        
        logger.info("JobManagementInterceptor called from method {} with {} parameters.", method, parameters != null ? parameters.length : 0);
        
        AuditLog log = new AuditLog();
        log.setMethod(method);
        log.setInvocationTime(new Date());
        
        Session session = ((EntityManagerImpl) em.getDelegate()).getSession();
        Transaction tx = session.beginTransaction();
        em.persist(log);
        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                AuditParameter parameter = new AuditParameter();
                parameter.setAuditLog(log);
                parameter.setParameterIndex(i);
                parameter.setValue(parameters[i].toString());
                parameter.setType(parameters[i].getClass().getSimpleName());
                em.persist(parameter);
            }
        }
        
        try {
            Object result = ctx.proceed();
            logger.info("{} terminated with {}...", method, result == null ? null : result.toString());
            log.setResult(result == null ? null : result.toString());
            return result;
        }
        catch (Exception exception) {
            logger.error("{} threw with {}...", method, exception.getClass().getName());
            log.setResult(exception.getClass().getName());
            throw exception;
        }
        finally {
            if (tx.isActive()) {
                tx.commit();
            }
        }
    }

}
