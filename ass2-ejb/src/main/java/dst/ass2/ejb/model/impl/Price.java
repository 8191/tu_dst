package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.model.IPrice;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * Represents a specific price for job execution.
 * 
 * @author Manuel Faux
 */
@Entity
public class Price implements IPrice, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(unique = true)
    private Integer nrOfHistoricalJobs;
    
    private BigDecimal price;
    
    public Price() {
    }
    
    public Price(Integer nrOfHistoricalJobs, BigDecimal price) {
        this.nrOfHistoricalJobs = nrOfHistoricalJobs;
        this.price = price;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getNrOfHistoricalJobs() {
        return nrOfHistoricalJobs;
    }

    @Override
    public void setNrOfHistoricalJobs(Integer nrOfHistoricalJobs) {
        this.nrOfHistoricalJobs = nrOfHistoricalJobs;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    @Override
    public String toString() {
        return String.format("[jobs <= %d cost %f]", nrOfHistoricalJobs, price);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Price) {
            Price priceobj = (Price)obj;
            return nrOfHistoricalJobs.equals(priceobj.nrOfHistoricalJobs) && price.equals(priceobj.price);
        }
        return false;
    }
    
}
