package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.IAuditParameter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * Represents the audit log.
 * 
 * @author Manuel Faux
 */
@Entity
public class AuditLog implements IAuditLog, Serializable {
    
    @Id
    @GeneratedValue
    private long id;
    
    private String method;
    
    @Column(name = "audit_result")
    private String result;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date invocationTime;
    
    @OneToMany(targetEntity = AuditParameter.class, mappedBy = "auditLog")
    private List<IAuditParameter> parameters;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public Date getInvocationTime() {
        return invocationTime;
    }

    @Override
    public void setInvocationTime(Date invocationTime) {
        this.invocationTime = invocationTime;
    }

    @Override
    public List<IAuditParameter> getParameters() {
        return parameters;
    }

    @Override
    public void setParameters(List<IAuditParameter> parameters) {
        this.parameters = parameters;
    }

}
