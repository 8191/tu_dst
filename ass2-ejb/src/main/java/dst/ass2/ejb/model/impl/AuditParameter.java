package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.IAuditParameter;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Represents parameters of an audit log row.
 * 
 * @author Manuel Faux
 */
@Entity
@Table(name = "auditlog_parameter")
public class AuditParameter implements IAuditParameter, Serializable {

    @Id
    @GeneratedValue
    private long id;
    
    private Integer parameterIndex;
    
    @Column(name = "parameter_type")
    private String type;
    
    @Column(name = "parameter_value")
    private String value;
    
    @ManyToOne(targetEntity = AuditLog.class)
    private IAuditLog auditLog;
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getParameterIndex() {
        return parameterIndex;
    }

    @Override
    public void setParameterIndex(Integer parameterIndex) {
        this.parameterIndex = parameterIndex;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public IAuditLog getAuditLog() {
        return auditLog;
    }

    @Override
    public void setAuditLog(IAuditLog auditLog) {
        this.auditLog = auditLog;
    }
    
}
