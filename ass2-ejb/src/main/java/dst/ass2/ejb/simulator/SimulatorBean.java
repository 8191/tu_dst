
package dst.ass2.ejb.simulator;

import dst.ass1.jpa.model.JobStatus;
import dst.ass1.jpa.model.impl.Execution;
import java.util.Date;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.EntityManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Startup
@Singleton
public class SimulatorBean {
    
    private static Logger logger = LoggerFactory.getLogger(SimulatorBean.class);
    
    @PersistenceContext
    private EntityManager em;
    
    @Schedule(second = "*/15", minute = "*", hour = "*")
    public void simulate() {
        
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        logger.info("Finishing jobs...");
        
        Transaction tx = session.beginTransaction();
        Criteria executionQuery = session.createCriteria(Execution.class)
                .add(Restrictions.ne("status", JobStatus.FINISHED))
                .add(Restrictions.isNotNull("start"));
        List<Execution> executions = (List<Execution>)executionQuery.list();
        for (Execution execution : executions) {
            execution.setStatus(JobStatus.FINISHED);
            execution.setEnd(new Date());
        }
        
        tx.commit();
    }
}
