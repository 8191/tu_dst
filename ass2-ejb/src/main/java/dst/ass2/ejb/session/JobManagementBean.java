package dst.ass2.ejb.session;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.model.impl.User;
import java.util.List;

import dst.ass2.ejb.dto.AssignmentDTO;
import dst.ass2.ejb.interceptor.AuditInterceptor;
import dst.ass2.ejb.session.exception.AssignmentException;
import dst.ass2.ejb.session.interfaces.IJobManagementBean;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.EntityManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateful
@Interceptors({AuditInterceptor.class})
public class JobManagementBean implements IJobManagementBean {

    private static Logger logger = LoggerFactory.getLogger(JobManagementBean.class);
    
    @PersistenceContext
    private EntityManager em;
    
    /**
     * Cache of free computers per grid.
     */
    private Map<Long, List<IComputer>> freeResources;
    
    /**
     * Queue of unsubmitted jobs.
     */
    private List<AssignmentDTO> queue;
    
    /**
     * Logged in user or <code>null</code> if no login was processed so far.
     */
    private IUser user;

    @PostConstruct
    void initialize() {
        user = null;

        freeResources = new HashMap<Long, List<IComputer>>();
        queue = new ArrayList<AssignmentDTO>();

        logger.info("JobManagementBean created.");
    }
    
    private void getFreeResources(Long gridId) {
        // Query available CPUs for grid, if this is the first add call for this grid
        if (!freeResources.containsKey(gridId)) {
            TypedQuery<IComputer> query = em.createNamedQuery("findFreeComputers", IComputer.class);
            query.setParameter("grid_id", gridId);

            List<IComputer> computers = query.getResultList();
            logger.info("Retrieved {} free computers for grid {}.", computers.size(), gridId);
            freeResources.put(gridId, computers);
        }
    }
    
    private boolean gridHasResources(Long gridId, Integer numCPUs) {
        // Cound if available grid CPUs are sufficient for this job
        int freeCPUs = 0;
        for (IComputer computer : freeResources.get(gridId)) {
            freeCPUs += computer.getCpus();
        }
        logger.info("Grid {} has {} free computers and {} free CPUs.", new Object[]{gridId, freeResources.get(gridId).size(), freeCPUs});
        return freeCPUs >= numCPUs;
    }

    @Override
    public void addJob(Long gridId, Integer numCPUs, String workflow,
            List<String> params) throws AssignmentException {

        // Query available CPUs for grid, if this is the first add call for this grid
        getFreeResources(gridId);
        
        logger.info("Trying to add job with {} CPUs to grid {}.", numCPUs, gridId);
        
        if (!gridHasResources(gridId, numCPUs)) {
            logger.warn("Grid {} does not have free {} CPUs.", gridId, numCPUs);
            throw new AssignmentException("Not enough free CPUs available in cluster.");
        }
        
        // Add job to queue
        AssignmentDTO assignmentDTO = new AssignmentDTO(gridId, numCPUs, workflow, params, new ArrayList<Long>());
        queue.add(assignmentDTO);
        
        // Assign computers to job
        int CPUsNeeded = numCPUs;
        for (Iterator<IComputer> it = freeResources.get(gridId).iterator(); CPUsNeeded > 0 && it.hasNext();) {
            IComputer computer = it.next();
            CPUsNeeded -= computer.getCpus();
            it.remove();
            assignmentDTO.getComputerIds().add(computer.getId());
        }
        assert numCPUs <= 0;
        
        logger.info("Queued job with {} CPUs for grid {} with {} computers.", new Object[] {numCPUs, gridId, assignmentDTO.getComputerIds().size()});
    }

    @Override
    public void login(String username, String password)
            throws AssignmentException {
        
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger(JobManagementBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new AssignmentException("Error querying user database.");
        }
        
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        Criteria userQuery = session.createCriteria(User.class)
                .add(Restrictions.eq("username", username))
                .add(Restrictions.eq("password", md.digest(password.getBytes())));
        
        user = (IUser)userQuery.uniqueResult();
        if (user == null) {
            throw new AssignmentException("Could not login user.");
        }
    }

    @Override
    public void removeJobsForGrid(Long gridId) {
        // Remove all relevant jobs from queue
        for (Iterator<AssignmentDTO> it = queue.iterator(); it.hasNext();) {
            AssignmentDTO assignmentDTO = it.next();
            if (assignmentDTO.getGridId().equals(gridId)) {
                it.remove();
            }
        }
        
        // Clear free ressources cache, since previously assigned computers
        // need to be populated into this list again.
        freeResources.remove(gridId);
    }

    @Override
    @Remove(retainIfException = true)
    public void submitAssignments() throws AssignmentException {        
        if (user == null)
            throw new AssignmentException("User not logged in.");
        
        freeResources.clear();
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        Transaction tx = session.beginTransaction();
                
        try {
            for (AssignmentDTO assignmentDTO : queue) {
                logger.info("Creating job for grid {} with {} computers.", assignmentDTO.getGridId(), assignmentDTO.getComputerIds().size());
                // Obtain current ressource values for this grid
                getFreeResources(assignmentDTO.getGridId());
                
                // Create environment
                IEnvironment environment = ModelFactory.createEnvironment();
                environment.setWorkflow(assignmentDTO.getWorkflow());
                environment.setParams(assignmentDTO.getParams());
                // Create execution
                IExecution execution = ModelFactory.createExecution();
                execution.setStart(new Date());
                execution.setStatus(JobStatus.SCHEDULED);
                // Create job
                IJob job = ModelFactory.createJob();
                job.setEnvironment(environment);
                job.setUser(user);
                job.setExecution(execution);
                // Persist job, environment and execution are cascaded
                em.persist(job);
                
                // Add Execution to each computer and check free ressources
                int computerCPUs = 0;
                for (Long computerId : assignmentDTO.getComputerIds()) {
                    logger.info("Assign job {} to computer {}.", job.getId(), computerId);
                    IComputer computer = null;
                    for (IComputer ccheck : freeResources.get(assignmentDTO.getGridId())) {
                        if (ccheck.getId().equals(computerId)) {
                            computer = ccheck;
                            break;
                        }
                    }
                    
                    if (computer == null) {
                        logger.warn("Assigned computer {} not available anymore.", computerId);
                        throw new AssignmentException("Assigned computers not available anymore.");
                    }
                    
                    execution.addComputer(computer);
                    //computer.addExecution(execution);
                    computerCPUs += computer.getCpus();
                }
                
                if (computerCPUs < assignmentDTO.getNumCPUs())
                    throw new AssignmentException("Not enought ressources available any more.");
            }
            
            tx.commit();
        }
        catch (AssignmentException e) {
            logger.error("Free CPUs of JobManagement cache not consistent with DB. Rolling back.");
            tx.rollback();
            throw e;
        }
    }

    @Override
    public List<AssignmentDTO> getCache() {        
        return queue;
    }
}
