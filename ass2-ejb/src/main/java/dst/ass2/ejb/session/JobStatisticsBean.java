package dst.ass2.ejb.session;

import dst.ass1.jpa.model.JobStatus;
import dst.ass1.jpa.model.impl.Execution;
import dst.ass1.jpa.model.impl.Grid;
import dst.ass2.ejb.dto.StatisticsDTO;
import dst.ass2.ejb.session.exception.WebServiceException;
import dst.ass2.ejb.session.interfaces.IJobStatisticsBean;
import dst.ass2.ejb.ws.Constants;
import dst.ass2.ejb.ws.IGetStatsRequest;
import dst.ass2.ejb.ws.IGetStatsResponse;
import dst.ass2.ejb.ws.impl.GetStatsResponse;
import java.util.List;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.soap.Addressing;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.EntityManagerImpl;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@WebService(serviceName = Constants.SERVICE_NAME, name = Constants.NAME, portName = Constants.PORT_NAME, targetNamespace = Constants.NAMESPACE)
@Addressing
public class JobStatisticsBean implements IJobStatisticsBean {

    private static Logger logger = LoggerFactory.getLogger(JobStatisticsBean.class);
    
    @PersistenceContext
    private EntityManager em;

    @Override
    @WebMethod(operationName = "getStatisticsForGrid")
    @Action(input = Constants.NAMESPACE + "/" + Constants.NAME + "/getStatisticsForGridRequest",
            output = Constants.NAMESPACE + "/" + Constants.NAME + "/getStatisticsForGridResponse",
            fault = {
                @FaultAction(className = WebServiceException.class,
                    value = Constants.NAMESPACE + "/" + Constants.NAME + "/getStatisticsForGrid/Fault/WebServiceException")
            })
    public IGetStatsResponse getStatisticsForGrid(IGetStatsRequest request,
            @WebParam(name = "gridName", header = true, partName = "gridName") String gridName) throws WebServiceException {
        logger.info("Getting statistics for grid {}.", gridName);
        logger.info("Statistics limit of {} requested.", request.getMaxExecutions());
        Session session = ((EntityManagerImpl) em.getDelegate()).getSession();
        
        Criteria grid = session.createCriteria(Grid.class)
                .add(Restrictions.eq("name", gridName));
        if (grid.uniqueResult() == null) {
            throw new WebServiceException(String.format("Grid %s does not exist.", gridName));
        }

        Criteria stats = session.createCriteria(Execution.class)
                .createAlias("computers", "c")
                .createAlias("c.cluster", "cl")
                .createAlias("cl.grid", "g")
                .add(Restrictions.eq("g.name", gridName))
                .add(Restrictions.eq("status", JobStatus.FINISHED))
                .setMaxResults(request.getMaxExecutions())
                .setFetchMode("computers", FetchMode.JOIN)
                .setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);

        StatisticsDTO statistics = new StatisticsDTO();
        statistics.setName(gridName);
        for (Execution execution : (List<Execution>)stats.list()) {
            statistics.addExecution(execution);
        }
        logger.info("Returning: {}", statistics);
        return new GetStatsResponse(statistics);
    }
}
