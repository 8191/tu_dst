package dst.ass2.ejb.session.interfaces;

import javax.ejb.Remote;

@Remote
public interface ITestingBean {
	
	/**
	 * Adds the test-data to the database.
	 */
	public void insertTestData();
	
}
