package dst.ass2.ejb.session;

import dst.ass1.jpa.model.IGrid;
import dst.ass1.jpa.model.IJob;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.JobStatus;
import dst.ass1.jpa.model.impl.Job;
import dst.ass1.jpa.model.impl.User;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Future;

import dst.ass2.ejb.dto.AuditLogDTO;
import dst.ass2.ejb.dto.BillDTO;
import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.impl.AuditLog;
import dst.ass2.ejb.session.exception.AssignmentException;

import dst.ass2.ejb.session.interfaces.IGeneralManagementBean;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.EntityManagerImpl;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used for general management.
 * 
 * Session Bean is stateless, since no method shares states.
 */
@Stateless
public class GeneralManagementBean implements IGeneralManagementBean {
    
    private static Logger logger = LoggerFactory.getLogger(GeneralManagementBean.class);
	
    @EJB
    private IPriceManagementBean priceManagementBean;
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addPrice(Integer nrOfHistoricalJobs, BigDecimal price) {
        priceManagementBean.setPrice(nrOfHistoricalJobs, price);
    }

    @Override
    @Asynchronous
    public Future<BillDTO> getBillForUser(String username) throws Exception {
        BillDTO bill = new BillDTO();
        bill.setUsername(username);
        
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        
        Criteria user = session.createCriteria(User.class)
                .add(Restrictions.eq("username", username));
        if (user.uniqueResult() == null) {
            throw new AssignmentException("User does not exist.");
        }
        
        Transaction tx = session.beginTransaction();

        // Query grid discounts
        HashMap<IGrid, Double> discounts = new HashMap<IGrid, Double>();
        Query memberships = session.createQuery("select m from Membership m join m.id.user u join fetch m.id.grid where u.username = :username");
        memberships.setString("username", username);
        for (IMembership membership : (List<IMembership>)memberships.list()) {
            discounts.put(membership.getId().getGrid(), membership.getDiscount());
        }
        
        // Query amount of paid jobs
        Integer paidJobs = ((Number) session.createCriteria(Job.class)
                .createAlias("user", "user")
                .add(Restrictions.eq("isPaid", true))
                .add(Restrictions.eq("user.username", username))
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();
        logger.info("User has initially {} jobs paid.", paidJobs);
        
        // Query unpaid, finished jobs
        Criteria jobs = session.createCriteria(Job.class)
                .createAlias("user", "user")
                .createAlias("execution", "execution")
                .add(Restrictions.eq("user.username", username))
                .add(Restrictions.eq("execution.status", JobStatus.FINISHED))
                .add(Restrictions.eq("isPaid", false))
                .setFetchMode("execution.computers.cluster.grid", FetchMode.JOIN);
        List<BillDTO.BillPerJob> billList = new ArrayList<BillDTO.BillPerJob>();
        // Generate bill rows
        BigDecimal totalPrice = new BigDecimal(0);
        for (IJob job : (List<IJob>)jobs.list()) {
            logger.info("Processing job {}...", job.getId());
            BigDecimal setupCosts = priceManagementBean.getPrice(paidJobs);
            if (setupCosts == null) {
                throw new AssignmentException("No prices are set.");
            }
            
            IGrid grid = job.getExecution().getComputers().get(0).getCluster().getGrid();
            BigDecimal discount = BigDecimal.valueOf(1).subtract(BigDecimal.valueOf(discounts.get(grid)));
            BillDTO.BillPerJob jobBill = bill.new BillPerJob();
            
            // Populate bill row
            jobBill.setJobId(job.getId());
            jobBill.setNumberOfComputers(job.getExecution().getComputers().size());
            jobBill.setSetupCosts(setupCosts.multiply(discount)
                    .setScale(2, RoundingMode.UP));
            jobBill.setExecutionCosts(grid.getCostsPerCPUMinute()
                    .multiply(BigDecimal.valueOf(job.getExecutionTime()))
                    .multiply(discount)
                    .setScale(2, RoundingMode.UP));
            // Calculate total costs of this job
            jobBill.setJobCosts(jobBill.getExecutionCosts()
                    .add(jobBill.getSetupCosts())
                    .setScale(2, RoundingMode.UP));
            // Add bill row to bill and set job paid
            billList.add(jobBill);
            totalPrice = totalPrice.add(jobBill.getJobCosts());
            job.setPaid(true);
            paidJobs++;
            em.persist(job);
        }
        bill.setBills(billList);
        bill.setTotalPrice(totalPrice);
        
        tx.commit();
        return new AsyncResult<BillDTO>(bill);
    }

    @Override
    public List<AuditLogDTO> getAuditLogs() {
        em.clear();
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        Criteria audits = session.createCriteria(AuditLog.class)
                .setFetchMode("parameters", FetchMode.JOIN)
                .setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        List<AuditLogDTO> result = new ArrayList<AuditLogDTO>();
        for (IAuditLog log : (List<IAuditLog>) audits.list()) {
            result.add(new AuditLogDTO(log));
        }
        logger.info("Retrieved {} audit log results.", result.size());
        return result;
    }

    @Override
    public void clearPriceCache() {
        priceManagementBean.clearCache();
    }
}
