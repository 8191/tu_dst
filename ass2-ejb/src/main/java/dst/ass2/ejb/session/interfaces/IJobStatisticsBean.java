package dst.ass2.ejb.session.interfaces;

import dst.ass2.ejb.session.exception.WebServiceException;
import dst.ass2.ejb.ws.Constants;
import dst.ass2.ejb.ws.IGetStatsRequest;
import dst.ass2.ejb.ws.IGetStatsResponse;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * This is the interface of the JobStatistics Web Service.
 */
@WebService(name = Constants.NAME, serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.NAMESPACE)
@SOAPBinding
public interface IJobStatisticsBean {

    /**
     * Get statistics for a given grid.
     *
     * @param request The request object with parameters
     * @param request Name of the grid
     * @return statistics for the grid with the specified name.
     */
    @WebMethod(operationName = "getStatisticsForGrid")
    IGetStatsResponse getStatisticsForGrid(IGetStatsRequest request,
        @WebParam(name = "gridName", header = true, partName = "gridName") String gridName) throws WebServiceException;
}
