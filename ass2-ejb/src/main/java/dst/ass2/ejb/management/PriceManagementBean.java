package dst.ass2.ejb.management;

import java.math.BigDecimal;

import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.impl.Price;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.ejb.EntityManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Startup
@Singleton
public class PriceManagementBean implements IPriceManagementBean {
    
    private static Logger logger = LoggerFactory.getLogger(PriceManagementBean.class);
    
    @PersistenceContext
    protected EntityManager em;
    
    private List<Price> pricesCache;
    
    @PostConstruct
    void initialize() {
        getPrices();
        logger.info("Singleton PriceManagementBean created.");
    }
    
    private void getPrices() {
        logger.info("Loading prices cache from database.");
        Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
        Criteria criteria = session.createCriteria(Price.class).addOrder(Order.asc("nrOfHistoricalJobs"));
        pricesCache = criteria.list();
    }

    @Override
    public BigDecimal getPrice(Integer nrOfHistoricalJobs) {
        logger.info("Retrieving price for {} jobs.", nrOfHistoricalJobs);
        BigDecimal result = null;
        for (Price price : pricesCache) {
            if (price.getNrOfHistoricalJobs().compareTo(nrOfHistoricalJobs) <= 0)
                result = price.getPrice();
            else
                break;
        }
        
        logger.info("Price for {} jobs is {}", nrOfHistoricalJobs, result);
        
        return result;
    }

    @Override
    public void setPrice(Integer nrOfHistoricalJobs, BigDecimal price) {
        logger.info("Requested to set price of {} jobs to {}.", nrOfHistoricalJobs, price);
        
        Price cprice = new Price(nrOfHistoricalJobs, price);
        if (pricesCache.contains(cprice)) {
            logger.info("Price for {} jobs already at value {}.", nrOfHistoricalJobs, price);
        }
        else {
            Session session = ((EntityManagerImpl)em.getDelegate()).getSession();
            Transaction tx = session.beginTransaction();
            
            cprice.setPrice(null);
            Criteria criteria = session.createCriteria(Price.class).add(Example.create(cprice));
            cprice = (Price)criteria.uniqueResult();
            if (cprice == null) {
                logger.info("Price of {} jobs not set so far. Creating.", nrOfHistoricalJobs);
                cprice = new Price(nrOfHistoricalJobs, price);
                em.persist(cprice);
            }
            else {
                logger.info("Updating price of {} jobs from {} to {}.", new Object[] {nrOfHistoricalJobs, cprice.getPrice(), price});
                cprice.setPrice(price);
                //em.flush();
            }

            tx.commit();
            getPrices();
        }
    }

    @Override
    public void clearCache() {
        logger.info("Clearing cache.");
        getPrices();
    }
}
