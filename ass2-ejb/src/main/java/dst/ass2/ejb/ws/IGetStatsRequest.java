package dst.ass2.ejb.ws;

import dst.ass2.ejb.ws.impl.GetStatsRequest;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * This interface defines the getters and setters of the 
 * GetStatsRequest Web service request object.
 */
@XmlJavaTypeAdapter(GetStatsRequest.Adapter.class)
public interface IGetStatsRequest {

	/**
	 * @return maximum number of executions in the statistics
	 */
	int getMaxExecutions();

	/**
	 * @param maxExecutions maximum number of executions in the statistics
	 */
	void setMaxExecutions(int maxExecutions);

}
