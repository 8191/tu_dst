package dst.ass2.ejb.ws.impl;

import dst.ass2.ejb.dto.StatisticsDTO;
import dst.ass2.ejb.ws.IGetStatsResponse;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author Manuel Faux
 */
public class GetStatsResponse implements IGetStatsResponse{

    private StatisticsDTO statistics;
    
    public GetStatsResponse(StatisticsDTO statistics) {
        this.statistics = statistics;
    }
    
    public GetStatsResponse() {
    }
    
    public void setStatistics(StatisticsDTO statistics) {
        this.statistics = statistics;
    }
    
    @Override
    public StatisticsDTO getStatistics() {
        return statistics;
    }

    public static class Adapter extends XmlAdapter<GetStatsResponse, IGetStatsResponse> {

        @Override
        public IGetStatsResponse unmarshal(GetStatsResponse v) {
            return v;
        }

        @Override
        public GetStatsResponse marshal(IGetStatsResponse v) {
            return (GetStatsResponse) v;
        }
    }

}
