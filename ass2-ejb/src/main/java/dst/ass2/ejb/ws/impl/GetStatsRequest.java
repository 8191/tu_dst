package dst.ass2.ejb.ws.impl;

import dst.ass2.ejb.ws.IGetStatsRequest;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author Manuel Faux
 */
public class GetStatsRequest implements IGetStatsRequest {
    
    private int maxExecutions;

    @Override
    public int getMaxExecutions() {
        return maxExecutions;
    }

    @Override
    public void setMaxExecutions(int maxExecutions) {
        this.maxExecutions = maxExecutions;
    }

    public static class Adapter extends XmlAdapter<GetStatsRequest, IGetStatsRequest> {

        @Override
        public IGetStatsRequest unmarshal(GetStatsRequest v) {
            return v;
        }

        @Override
        public GetStatsRequest marshal(IGetStatsRequest v) {
            return (GetStatsRequest) v;
        }
    }
    
}
