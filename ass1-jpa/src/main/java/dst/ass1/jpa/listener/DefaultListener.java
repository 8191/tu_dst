package dst.ass1.jpa.listener;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.*;

/**
 * Listener which audits JPA backend operations.
 *
 * <p>
 * Needs to be referenced in ORM mapping configuration.
 * </p>
 * 
 * @author Manuel Faux
 */
public class DefaultListener {

    private static AtomicInteger loadOperations = new AtomicInteger(0);
    private static AtomicInteger updateOperations = new AtomicInteger(0);
    private static AtomicInteger removeOperations = new AtomicInteger(0);
    private static AtomicInteger persistOperations = new AtomicInteger(0);
    /**
     * Sum of all persist operations.
     */
    private static AtomicLong persistTime = new AtomicLong(0);
    /**
     * Hash map to handle asynchronous persist operations.
     */
    private static HashMap<Integer, Long> persistMap = new HashMap<Integer, Long>();

    /**
     * Count number of load operations.
     */
    @PostLoad
    public void postLoad(Object o) {
        loadOperations.getAndIncrement();
    }

    /**
     * COunt number of update operations.
     */
    @PostUpdate
    public void postUpdate(Object o) {
        updateOperations.getAndIncrement();
    }

    /**
     * Count number of remove operations.
     */
    @PostRemove
    public void postRemove(Object o) {
        removeOperations.getAndIncrement();
    }

    /**
     * Start measuring persistent timings by adding current object hash and
     * current time to tracking map.
     */
    @PrePersist
    public void prePersist(Object o) {
        persistMap.put(o.hashCode(), new Date().getTime());
    }

    /**
     * Calculate persistent timings by reading begin time from tracking map and
     * stop the timing time span.
     */
    @PostPersist
    public void postPersist(Object o) {
        long now = new Date().getTime();
        persistTime.getAndAdd(now - persistMap.remove(o.hashCode()));
        persistOperations.getAndIncrement();
    }

    public static int getLoadOperations() {
        return loadOperations.get();
    }

    public static int getUpdateOperations() {
        return updateOperations.get();
    }

    public static int getRemoveOperations() {
        return removeOperations.get();
    }

    public static int getPersistOperations() {
        return persistOperations.get();
    }

    public static long getOverallTimeToPersist() {
        return persistTime.get();
    }

    public static double getAverageTimeToPersist() {
        return (double) persistTime.get() / persistOperations.get();
    }

    /**
     * Resets all operation and time counters to zero.
     */
    public static void clear() {
        loadOperations.set(0);
        updateOperations.set(0);
        removeOperations.set(0);
        persistOperations.set(0);
        persistTime.set(0);
    }
}
