package dst.ass1.jpa.listener;

import dst.ass1.jpa.model.impl.Computer;
import java.util.Date;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

/**
 * Listener to maintain Computer entities before being stored in database.
 *
 * <p>
 * Listener needs to be referenced in entity definition.
 * </p>
 *
 * @author Manuel Faux
 */
public class ComputerListener {

    /**
     * Sets the creation and update dates for new Computer entities before being
     * persisted.
     */
    @PrePersist
    public void prePersist(Computer c) {
        c.setCreation(new Date());
        c.setLastUpdate(new Date());
    }

    /**
     * Updates the update date for Computer entities before being updated in the
     * database.
     */
    @PostUpdate
    public void preUpdate(Computer c) {
        c.setLastUpdate(new Date());
    }
}
