package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents an Execution entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Execution implements IExecution, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    @Enumerated(EnumType.STRING)
    private JobStatus status;
    
    @ManyToMany(targetEntity = Computer.class)
    private List<IComputer> computers;
    
    @OneToOne(targetEntity = Job.class, mappedBy = "execution", optional = false)
    private IJob job;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getStart() {
        return start;
    }

    @Override
    public void setStart(Date start) {
        this.start = start;
    }

    @Override
    public Date getEnd() {
        return end;
    }

    @Override
    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public JobStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(JobStatus status) {
        this.status = status;
    }

    @Override
    public List<IComputer> getComputers() {
        return computers;
    }

    @Override
    public void setComputers(List<IComputer> computers) {
        this.computers = computers;
    }

    @Override
    public void addComputer(IComputer computer) {
        if (computers == null) {
            computers = new ArrayList<IComputer>();
        }

        computers.add(computer);
    }

    @Override
    public IJob getJob() {
        return job;
    }

    @Override
    public void setJob(IJob job) {
        this.job = job;
    }
}
