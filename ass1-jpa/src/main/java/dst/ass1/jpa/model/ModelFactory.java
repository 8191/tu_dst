package dst.ass1.jpa.model;

import dst.ass1.jpa.model.impl.*;
import java.io.Serializable;

public class ModelFactory implements Serializable {

	private static final long serialVersionUID = 1L;

	public static IAddress createAddress() {
		return new Address();
	}

	public static IAdmin createAdmin() {
		return new Admin();
	}

	public static ICluster createCluster() {
		return new Cluster();
	}

	public static IComputer createComputer() {
		return new Computer();
	}

	public static IEnvironment createEnvironment() {
		return new Environment();
	}

	public static IExecution createExecution() {
		return new Execution();
	}

	public static IGrid createGrid() {
		return new Grid();
	}

	public static IJob createJob() {
		return new Job();
	}

	public static IMembership createMembership() {
		return new Membership();
	}

	public static IMembershipKey createMembershipKey() {
		return new MembershipKey();
	}

	public static IUser createUser() {
		return new User();
	}

}
