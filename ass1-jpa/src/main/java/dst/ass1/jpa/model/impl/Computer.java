package dst.ass1.jpa.model.impl;

import javax.persistence.*;
import dst.ass1.jpa.listener.ComputerListener;
import dst.ass1.jpa.model.*;
import dst.ass1.jpa.validator.CPUs;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityListeners;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Represents a Computer entity.
 * 
 * @author Manuel Faux
 */
@Entity
@EntityListeners(ComputerListener.class)
@NamedQueries({
    @NamedQuery(name = "findComputersInVienna", query = "select c from Computer c where c.location like 'AUT-VIE%'")
})
public class Computer implements IComputer, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Size(min = 5, max = 25)
    @Column(unique = true)
    private String name;
    
    @CPUs(min = 4, max = 8)
    private Integer cpus;
    
    @Pattern(regexp = "[A-Z]{3}-[A-Z]{3}@[0-9]{4,}")
    private String location;
    
    @Past
    @Temporal(TemporalType.DATE)
    private Date creation;
    
    @Past
    @Temporal(TemporalType.DATE)
    private Date lastUpdate;
    
    @ManyToOne(targetEntity = Cluster.class)
    private ICluster cluster;
    
    @ManyToMany(targetEntity = Execution.class, mappedBy = "computers")
    private List<IExecution> executions;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getCpus() {
        return cpus;
    }

    @Override
    public void setCpus(Integer cpus) {
        this.cpus = cpus;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public Date getCreation() {
        return creation;
    }

    @Override
    public void setCreation(Date creation) {
        this.creation = creation;
    }

    @Override
    public Date getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public ICluster getCluster() {
        return cluster;
    }

    @Override
    public void setCluster(ICluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public List<IExecution> getExecutions() {
        return executions;
    }

    @Override
    public void setExecutions(List<IExecution> executions) {
        this.executions = executions;
    }

    @Override
    public void addExecution(IExecution execution) {
        if (executions == null) {
            executions = new ArrayList<IExecution>();
        }

        executions.add(execution);
    }
}
