package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.annotations.Index;

/**
 * Represents a User entity.
 * 
 * @author Manuel Faux
 */
@Entity
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"accountNo", "bankCode"})
)
@NamedQueries({
    @NamedQuery(
        // Returns all users who have an active membership for a specifc grid (:name)
        // and have at least a minimum amount of jobs associated (:minNr).
        name = "findUsersWithActiveMembership",
        // Tests do not pass, since :minNr parameter is passed as Long but size requires an Integer for comparison
        //query = "select m.id.user from Membership m where m.id.grid.name = :name and m.id.user.jobs.size >= :minNr"
        query = "select m.id.user from Membership m join m.id.user.jobs j where m.id.grid.name = :name group by m.id.user having count(j) >= :minNr"
    ),
    @NamedQuery(
        // Returns the users with the most jobs associated.
        name = "findMostActiveUser",
        query = "select j.user from Job j group by j.user having count(j.user) = (select max(u.jobs.size) from User u)"
    )
})
public class User extends Person implements IUser, Serializable {

    @Column(nullable = false, unique = true)
    private String username;
    
    @Column(columnDefinition = "binary(16)")
    @Index(name = "password")
    private byte[] password;
    
    private String accountNo;
    
    private String bankCode;
    
    @OneToMany(targetEntity = Job.class, mappedBy = "user")
    private List<IJob> jobs;
    
    @OneToMany(targetEntity = Membership.class, mappedBy = "id.user")
    private List<IMembership> memberships;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public byte[] getPassword() {
        return password;
    }

    @Override
    public void setPassword(byte[] password) {
        this.password = password;
    }
                
    @Override
    public String getAccountNo() {
        return accountNo;
    }

    @Override
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public String getBankCode() {
        return bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public List<IJob> getJobs() {
        return jobs;
    }

    @Override
    public void setJobs(List<IJob> jobs) {
        this.jobs = jobs;
    }

    @Override
    public void addJob(IJob job) {
        if (jobs == null) {
            jobs = new ArrayList<IJob>();
        }

        jobs.add(job);
    }

    @Override
    public void addMembership(IMembership membership) {
        if (memberships == null) {
            memberships = new ArrayList<IMembership>();
        }

        memberships.add(membership);
    }

    @Override
    public List<IMembership> getMemberships() {
        return memberships;
    }

    @Override
    public void setMemberships(List<IMembership> memberships) {
        this.memberships = memberships;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
