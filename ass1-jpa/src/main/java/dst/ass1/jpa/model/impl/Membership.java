package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Represents a Membership entity.
 * 
 * <p>
 * The membership entity realizes the n:m relation between the Grid and the User
 * entities.
 * </p>
 * 
 * @author Manuel Faux
 */
@Entity
public class Membership implements IMembership, Serializable {

    @EmbeddedId
    private MembershipKey id;
    
    @Temporal(TemporalType.DATE)
    private Date registration;
    
    private Double discount;

    @Override
    public IMembershipKey getId() {
        return id;
    }

    @Override
    public void setId(IMembershipKey id) {
        this.id = (MembershipKey) id;
    }

    @Override
    public Date getRegistration() {
        return registration;
    }

    @Override
    public void setRegistration(Date registration) {
        this.registration = registration;
    }

    @Override
    public Double getDiscount() {
        return discount;
    }

    @Override
    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
