package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Admin entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Admin extends Person implements IAdmin, Serializable {

    @OneToMany(targetEntity = Cluster.class, mappedBy = "admin")
    private List<ICluster> clusters;

    @Override
    public List<ICluster> getClusters() {
        return clusters;
    }

    @Override
    public void setClusters(List<ICluster> clusters) {
        this.clusters = clusters;
    }

    @Override
    public void addCluster(ICluster cluster) {
        if (clusters == null) {
            clusters = new ArrayList<ICluster>();
        }

        clusters.add(cluster);
    }
}
