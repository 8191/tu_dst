package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Grid entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Grid implements IGrid, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(unique = true)
    private String name;
    
    private String location;
    
    private BigDecimal costsPerCPUMinute;
    
    @OneToMany(targetEntity = Membership.class, mappedBy = "id.grid")
    private List<IMembership> memberships;
    
    @OneToMany(targetEntity = Cluster.class, mappedBy = "grid")
    private List<ICluster> clusters;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public BigDecimal getCostsPerCPUMinute() {
        return costsPerCPUMinute;
    }

    @Override
    public void setCostsPerCPUMinute(BigDecimal costsPerCPUMinute) {
        this.costsPerCPUMinute = costsPerCPUMinute;
    }

    @Override
    public void addMembership(IMembership membership) {
        if (memberships == null) {
            memberships = new ArrayList<IMembership>();
        }

        memberships.add(membership);
    }

    @Override
    public List<IMembership> getMemberships() {
        return memberships;
    }

    @Override
    public void setMemberships(List<IMembership> memberships) {
        this.memberships = memberships;
    }

    @Override
    public List<ICluster> getClusters() {
        return clusters;
    }

    @Override
    public void setClusters(List<ICluster> clusters) {
        this.clusters = clusters;
    }

    @Override
    public void addCluster(ICluster cluster) {
        if (clusters == null) {
            clusters = new ArrayList<ICluster>();
        }

        clusters.add(cluster);
    }
}
