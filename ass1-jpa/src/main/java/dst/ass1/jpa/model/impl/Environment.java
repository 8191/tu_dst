package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Environment entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Environment implements IEnvironment, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    private String workflow;
    
    @ElementCollection
    @OrderColumn
    private List<String> params;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getWorkflow() {
        return workflow;
    }

    @Override
    public void setWorkflow(String workflow) {
        this.workflow = workflow;
    }

    @Override
    public List<String> getParams() {
        return params;
    }

    @Override
    public void setParams(List<String> params) {
        this.params = params;
    }

    @Override
    public void addParam(String param) {
        if (params == null) {
            params = new ArrayList<String>();
        }

        params.add(param);
    }
}
