package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a Cluster entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Cluster implements ICluster, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Column(unique = true)
    private String name;
    
    @Temporal(TemporalType.DATE)
    private Date lastService;
    
    @Temporal(TemporalType.DATE)
    private Date nextService;
    
    @ManyToMany(targetEntity = Cluster.class)
    @JoinTable(name = "composed_of")
    private List<ICluster> composedOf;
    
    @ManyToMany(targetEntity = Cluster.class, mappedBy = "composedOf")
    private List<ICluster> partOf;
    
    @OneToMany(targetEntity = Computer.class)
    private List<IComputer> computers;
    
    @ManyToOne(targetEntity = Admin.class)
    private IAdmin admin;
    
    @ManyToOne(targetEntity = Grid.class)
    private IGrid grid;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Date getLastService() {
        return lastService;
    }

    @Override
    public void setLastService(Date lastService) {
        this.lastService = lastService;
    }

    @Override
    public Date getNextService() {
        return nextService;
    }

    @Override
    public void setNextService(Date nextService) {
        this.nextService = nextService;
    }

    @Override
    public List<ICluster> getComposedOf() {
        return composedOf;
    }

    @Override
    public void setComposedOf(List<ICluster> composedOf) {
        this.composedOf = composedOf;
    }

    @Override
    public void addComposedOf(ICluster cluster) {
        if (composedOf == null) {
            composedOf = new ArrayList<ICluster>();
        }

        composedOf.add(cluster);
    }

    @Override
    public List<ICluster> getPartOf() {
        return partOf;
    }

    @Override
    public void setPartOf(List<ICluster> partOf) {
        this.partOf = partOf;
    }

    @Override
    public void addPartOf(ICluster cluster) {
        if (partOf == null) {
            partOf = new ArrayList<ICluster>();
        }

        partOf.add(cluster);
    }

    @Override
    public List<IComputer> getComputers() {
        return computers;
    }

    @Override
    public void setComputers(List<IComputer> computers) {
        this.computers = computers;
    }

    @Override
    public void addComputer(IComputer computer) {
        if (computers == null) {
            computers = new ArrayList<IComputer>();
        }

        computers.add(computer);
    }

    @Override
    public IAdmin getAdmin() {
        return admin;
    }

    @Override
    public void setAdmin(IAdmin admin) {
        this.admin = admin;
    }

    @Override
    public IGrid getGrid() {
        return grid;
    }

    @Override
    public void setGrid(IGrid grid) {
        this.grid = grid;
    }
}
