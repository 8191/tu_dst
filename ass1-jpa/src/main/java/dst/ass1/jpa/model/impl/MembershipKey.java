package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Represents the key class for the Membership entity. The class is used to
 * realize a combined key using the User and Grid entities.
 * 
 * @author Manuel Faux
 */
@Embeddable
public class MembershipKey implements IMembershipKey, Serializable {

    @ManyToOne(targetEntity = User.class)
    private IUser user;
    
    @ManyToOne(targetEntity = Grid.class)
    private IGrid grid;

    public MembershipKey() {
    }

    public MembershipKey(IUser user, IGrid grid) {
        this.user = user;
        this.grid = grid;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user = user;
    }

    @Override
    public IGrid getGrid() {
        return grid;
    }

    @Override
    public void setGrid(IGrid grid) {
        this.grid = grid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof MembershipKey) {
            MembershipKey other = (MembershipKey) obj;

            return (user == null ? other.user == null : user.equals(other.user))
                    && (grid == null ? other.grid == null : grid.equals(other.grid));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (user == null ? 0 : user.hashCode()) ^ (grid == null ? 0 : grid.hashCode());
    }

    @Override
    public String toString() {
        return user.toString() + "-" + grid.toString();
    }
}
