package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;

/**
 * Represents a Person entity.
 * 
 * @author Manuel Faux
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person implements IPerson, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;
    
    protected String firstName;
    
    protected String lastName;
    
    @Embedded
    protected Address address;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public IAddress getAddress() {
        return address;
    }

    @Override
    public void setAddress(IAddress address) {
        this.address = (Address) address;
    }
}
