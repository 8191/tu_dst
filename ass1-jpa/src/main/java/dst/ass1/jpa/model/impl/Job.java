package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

/**
 * Represents a Job entity.
 * 
 * @author Manuel Faux
 */
@Entity
public class Job implements IJob, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    @Transient
    private Integer executionTime;
    
    @Transient
    private Integer numCPUs;
    
    private boolean isPaid;
    
    @OneToOne(targetEntity = Environment.class, optional = false, cascade = CascadeType.ALL)
    private IEnvironment environment;
    
    @ManyToOne(targetEntity = User.class, optional = false)
    private IUser user;
    
    @OneToOne(targetEntity = Execution.class, optional = false, cascade = CascadeType.ALL)
    private IExecution execution;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Calculate number of CPUs of associated computers.
     * 
     * <p>
     * The value is cached and only calculated once in an object lifetime.
     * </p>
     * 
     * @return the sum of CPUs of all computers associated with this job
     */
    @Override
    public Integer getNumCPUs() {
        if (numCPUs == null) {
            if (execution != null && execution.getComputers() != null) {
                int cpus = 0;
                for (Iterator<IComputer> it = execution.getComputers().iterator(); it.hasNext();) {
                    IComputer computer = it.next();
                    if (computer.getCpus() != null) {
                        cpus += computer.getCpus();
                    }
                }

                numCPUs = cpus;
            } else {
                numCPUs = 0;
            }
        }
        
        return numCPUs;
    }

    /**
     * Overrides the calculated, cached number of CPUs. Does not affect any
     * computer objects or database state.
     * 
     * @param numCPUs the value to override the number of CPUs with
     */
    @Override
    public void setNumCPUs(Integer numCPUs) {
        this.numCPUs = numCPUs;
    }

    /**
     * Calculate the execution time of the associated execution.
     * 
     * <p>
     * Time is calculated by end - start. The value is cached and only
     * calculated once in an object lifetime.
     * </p>
     * 
     * @return The execution time in minutes.
     */
    @Override
    public Integer getExecutionTime() {
        if (executionTime == null) {
            if (execution != null && execution.getEnd() != null) {
                executionTime = (int) (execution.getEnd().getTime() - execution.getStart().getTime()) / 60000;
            } else if (execution != null && execution.getStart() != null) {
                return (int) (new Date().getTime() - execution.getStart().getTime()) / 60000;
            } else {
                executionTime = 0;
            }
        }
        return executionTime;
    }

    /**
     * Overrides the calculated, cached execution time. Does not affect any
     * execution objects or database state.
     * 
     * @param executionTime the value to override the execution time with
     */
    @Override
    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public boolean isPaid() {
        return isPaid;
    }

    @Override
    public void setPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    @Override
    public IEnvironment getEnvironment() {
        return environment;
    }

    @Override
    public void setEnvironment(IEnvironment environment) {
        this.environment = environment;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user = user;
    }

    @Override
    public IExecution getExecution() {
        return execution;
    }

    @Override
    public void setExecution(IExecution execution) {
        this.execution = execution;
    }
}
