package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IExecutionDAO;
import dst.ass1.jpa.model.IExecution;
import dst.ass1.jpa.model.impl.Execution;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Execution entity.
 * 
 * @author Manuel Faux
 */
public class ExecutionDAO implements IExecutionDAO {

    private Session session;

    public ExecutionDAO(Session session) {
        this.session = session;
    }

    @Override
    public IExecution findById(Long id) {
        Criteria criteria = session.createCriteria(Execution.class).add(Restrictions.eq("id", id));
        return (IExecution) criteria.uniqueResult();
    }

    @Override
    public List<IExecution> findAll() {
        Criteria criteria = session.createCriteria(Execution.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
