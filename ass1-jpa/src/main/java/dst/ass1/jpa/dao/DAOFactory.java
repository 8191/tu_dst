package dst.ass1.jpa.dao;

import dst.ass1.jpa.dao.impl.*;

public class DAOFactory {

    private org.hibernate.Session session;

    public DAOFactory(org.hibernate.Session session) {
	this.session = session;

    }

    public IGridDAO getGridDAO() {
	return (IGridDAO) new GridDAO(session);
    }

    public IAdminDAO getAdminDAO() {
	return (IAdminDAO) new AdminDAO(session);
    }

    public IClusterDAO getClusterDAO() {
	return (IClusterDAO) new ClusterDAO(session);
    }

    public IComputerDAO getComputerDAO() {
	return (IComputerDAO) new ComputerDAO(session);
    }

    public IEnvironmentDAO getEnvironmentDAO() {
	return (IEnvironmentDAO) new EnvironmentDAO(session);
    }

    public IExecutionDAO getExecutionDAO() {
	return (IExecutionDAO) new ExecutionDAO(session);
    }

    public IJobDAO getJobDAO() {
	return (IJobDAO) new JobDAO(session);
    }

    public IMembershipDAO getMembershipDAO() {
	return (IMembershipDAO) new MembershipDAO(session);
    }

    public IUserDAO getUserDAO() {
	return (IUserDAO) new UserDAO(session);
    }
}
