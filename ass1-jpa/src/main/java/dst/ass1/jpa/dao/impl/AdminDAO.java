package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IAdminDAO;
import dst.ass1.jpa.model.IAdmin;
import dst.ass1.jpa.model.impl.Admin;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Admin entity.
 * 
 * @author Manuel Faux
 */
public class AdminDAO implements IAdminDAO {

    private Session session;

    public AdminDAO(Session session) {
        this.session = session;
    }

    @Override
    public IAdmin findById(Long id) {
        Criteria criteria = session.createCriteria(Admin.class).add(Restrictions.eq("id", id));
        return (IAdmin) criteria.uniqueResult();
    }

    @Override
    public List<IAdmin> findAll() {
        Criteria criteria = session.createCriteria(Admin.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
