package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IUserDAO;
import dst.ass1.jpa.model.IUser;
import dst.ass1.jpa.model.impl.User;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the User entity.
 * 
 * @author Manuel Faux
 */
public class UserDAO implements IUserDAO {

    private Session session;

    public UserDAO(Session session) {
        this.session = session;
    }

    @Override
    public IUser findById(Long id) {
        Criteria criteria = session.createCriteria(User.class).add(Restrictions.eq("id", id));
        return (IUser) criteria.uniqueResult();
    }

    @Override
    public List<IUser> findAll() {
        Criteria criteria = session.createCriteria(User.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
