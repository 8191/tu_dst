package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IEnvironmentDAO;
import dst.ass1.jpa.model.IEnvironment;
import dst.ass1.jpa.model.impl.Environment;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Environment entity.
 * 
 * @author Manuel Faux
 */
public class EnvironmentDAO implements IEnvironmentDAO {

    private Session session;

    public EnvironmentDAO(Session session) {
        this.session = session;
    }

    @Override
    public IEnvironment findById(Long id) {
        Criteria criteria = session.createCriteria(Environment.class).add(Restrictions.eq("id", id));
        return (IEnvironment) criteria.uniqueResult();
    }

    @Override
    public List<IEnvironment> findAll() {
        Criteria criteria = session.createCriteria(Environment.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
