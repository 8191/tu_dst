package dst.ass1.jpa.dao;

import dst.ass1.jpa.model.IJob;

import java.util.Date;
import java.util.List;

public interface IJobDAO extends GenericDAO<IJob> {

    /**
     * Query and return all jobs that execute a given workflow and that were
     * created by a specific user
     *
     * <p>
     * All parameters are optional. A
     * <code>null</code> value specifies to not include the value into the
     * query.
     * </p>
     *
     * @param user the user who created the job
     * @param workflow the workflow executed by the job
     * @return the jobs which match the given criteria
     */
    List<IJob> findJobsForUserAndWorkflow(String user, String workflow);

    /**
     * Query and return all finished jobs within specified time range.
     *
     * <p>
     * All parameters are optional. A
     * <code>null</code> value specifies to not include the value into the
     * query.
     * </p>
     *
     * @param start start time of job to query
     * @param finish end time of job to query
     * @return the jobs which match the given criteria
     */
    List<IJob> findJobForStatusFinishedStartandFinish(Date start, Date finish);
}
