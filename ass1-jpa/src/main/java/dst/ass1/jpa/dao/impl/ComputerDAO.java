package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IComputerDAO;
import dst.ass1.jpa.model.IComputer;
import dst.ass1.jpa.model.IExecution;
import dst.ass1.jpa.model.impl.Computer;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Computer entity.
 *
 * @author Manuel Faux
 */
public class ComputerDAO implements IComputerDAO {

    private Session session;

    public ComputerDAO(Session session) {
        this.session = session;
    }

    @Override
    public HashMap<IComputer, Integer> findComputersInViennaUsage() {
        List<IComputer> computers = session.getNamedQuery("findComputersInVienna").list();
        //List<IComputer> computers = findAll();
        HashMap<IComputer, Integer> usageMap = new HashMap<IComputer, Integer>(computers.size());

        for (IComputer computer : computers) {
            usageMap.put(computer, getUsage(computer));
        }

        return usageMap;
    }

    /**
     * Calculates the usage of a specific computer by summing up the current
     * execution duration of each associated job.
     *
     * @param computer the computer to calculate the usage of
     * @return the usage value
     */
    public Integer getUsage(IComputer computer) {
        Long usage = 0L;
        for (IExecution execution : computer.getExecutions()) {
            if (execution.getStart() != null && execution.getEnd() != null) {
                usage += execution.getEnd().getTime() - execution.getStart().getTime();
            }
        }

        if (usage > Integer.MAX_VALUE) {
            return null;
        }

        return (int) (long) usage;
    }

    @Override
    public IComputer findById(Long id) {
        Criteria criteria = session.createCriteria(Computer.class).add(Restrictions.eq("id", id));
        return (Computer) criteria.uniqueResult();
    }

    @Override
    public List<IComputer> findAll() {
        Criteria criteria = session.createCriteria(Computer.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
