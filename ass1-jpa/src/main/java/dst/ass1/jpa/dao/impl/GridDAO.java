package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IGridDAO;
import dst.ass1.jpa.model.IGrid;
import dst.ass1.jpa.model.impl.Grid;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Grid entity.
 * 
 * @author Manuel Faux
 */
public class GridDAO implements IGridDAO {

    private Session session;

    public GridDAO(Session session) {
        this.session = session;
    }

    @Override
    public IGrid findById(Long id) {
        Criteria criteria = session.createCriteria(Grid.class).add(Restrictions.eq("id", id));
        return (IGrid) criteria.uniqueResult();
    }

    @Override
    public List<IGrid> findAll() {
        Criteria criteria = session.createCriteria(Grid.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
