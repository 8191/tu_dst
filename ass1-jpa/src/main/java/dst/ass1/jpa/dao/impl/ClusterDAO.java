package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IClusterDAO;
import dst.ass1.jpa.model.ICluster;
import dst.ass1.jpa.model.impl.Cluster;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Cluster entity.
 * 
 * @author Manuel Faux
 */
public class ClusterDAO implements IClusterDAO {

    private Session session;

    public ClusterDAO(Session session) {
        this.session = session;
    }

    @Override
    public ICluster findById(Long id) {
        Criteria criteria = session.createCriteria(Cluster.class).add(Restrictions.eq("id", id));
        return (ICluster) criteria.uniqueResult();
    }

    @Override
    public List<ICluster> findAll() {
        Criteria criteria = session.createCriteria(Cluster.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
