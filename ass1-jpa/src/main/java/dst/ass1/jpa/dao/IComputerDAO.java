package dst.ass1.jpa.dao;

import dst.ass1.jpa.model.IComputer;

import java.util.HashMap;

public interface IComputerDAO extends GenericDAO<IComputer> {

    /**
     * Queries all computers in location <em>AUT-VIE</em> and calculates the
     * total computer usage.
     *
     * <p>
     * Total usage is sum of all current execution durations.
     * </p>
     *
     * @return Map of computers as a key and usage as a value.
     */
    public HashMap<IComputer, Integer> findComputersInViennaUsage();
}
