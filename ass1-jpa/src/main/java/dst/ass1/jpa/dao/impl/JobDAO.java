package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IJobDAO;
import dst.ass1.jpa.model.IExecution;
import dst.ass1.jpa.model.IJob;
import dst.ass1.jpa.model.JobStatus;
import dst.ass1.jpa.model.impl.Execution;
import dst.ass1.jpa.model.impl.Job;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Data access object to query the Job entity.
 * 
 * @author Manuel Faux
 */
public class JobDAO implements IJobDAO {

    private Session session;

    public JobDAO(Session session) {
        this.session = session;
    }

    @Override
    public IJob findById(Long id) {
        Criteria criteria = session.createCriteria(Job.class).add(Restrictions.eq("id", id));
        return (IJob) criteria.uniqueResult();
    }

    @Override
    public List<IJob> findAll() {
        Criteria criteria = session.createCriteria(Job.class).addOrder(Order.asc("id"));
        return criteria.list();
    }

    @Override
    public List<IJob> findJobsForUserAndWorkflow(String user, String workflow) {
        Criteria criteria = session.createCriteria(Job.class);
        // Not specified user value does not restrict query
        if (user != null && !user.equals("")) {
            criteria.createCriteria("user")
                    .add(Restrictions.eq("username", user));
        }
        // Not specified workflow value does not restrict query
        if (workflow != null && !workflow.equals("")) {
            criteria.createCriteria("environment")
                    .add(Restrictions.eq("workflow", workflow));
        }
        // Required by predefined JUnit test
        criteria.addOrder(Order.asc("id"));
        
        return criteria.list();
    }

    @Override
    public List<IJob> findJobForStatusFinishedStartandFinish(Date start, Date finish) {
        IExecution execution = new Execution();
        execution.setStatus(JobStatus.FINISHED);
        execution.setStart(start);
        execution.setEnd(finish);
        return session.createCriteria(Job.class)
                .createCriteria("execution").add(Example.create(execution))
                .list();
    }
}
