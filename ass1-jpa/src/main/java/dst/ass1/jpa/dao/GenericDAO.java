package dst.ass1.jpa.dao;

import java.util.List;

public interface GenericDAO<T> {

    /**
     * Queries and returns a specific entity identified by its id.
     *
     * @param id the id to identify the entity to query
     * @return the entity
     */
    T findById(Long id);

    /**
     * Queries and returns all entities in the persistence store.
     *
     * @return a list of all persisted entities
     */
    List<T> findAll();
}
