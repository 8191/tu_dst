package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMembershipDAO;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.impl.Membership;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 * Data access object to query the Membership entity.
 *
 * <p>
 * The membership entity realizes the n:m relation between the Grid and the User
 * entities.
 * </p>
 * 
 * @author Manuel Faux
 */
public class MembershipDAO implements IMembershipDAO {

    private Session session;

    public MembershipDAO(Session session) {
        this.session = session;
    }

    /**
     * This method is not supported for the Membership entity, since the id is
     * not a long value.
     *
     * @param id ignored
     * @throws UnsupportedOperationException every time the method is called
     */
    @Override
    public IMembership findById(Long id) {
        throw new UnsupportedOperationException("Membership uses a combined id");
    }

    @Override
    public List<IMembership> findAll() {
        Criteria criteria = session.createCriteria(Membership.class).addOrder(Order.asc("id"));
        return criteria.list();
    }
}
