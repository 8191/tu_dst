package dst.ass1.jpa.lifecycle;

import dst.ass1.jpa.model.IJob;
import dst.ass1.jpa.model.IUser;
import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;

import dst.ass1.jpa.model.ModelFactory;
import javax.persistence.EntityTransaction;

public class EMLifecycleDemo {

    private EntityManager em;
    private ModelFactory modelFactory;

    public EMLifecycleDemo(EntityManager em, ModelFactory modelFactory) {
        this.em = em;
        this.modelFactory = modelFactory;
    }

    /**
     * Method to illustrate the persistence lifecycle. EntityManager is opened
     * and closed by the Test-Environment!
     *
     * @throws NoSuchAlgorithmException
     */
    public void demonstrateEntityMangerLifecycle()
            throws NoSuchAlgorithmException {

        EntityTransaction tx = em.getTransaction();
        
        System.out.println("Creating new job...");
        IJob job = modelFactory.createJob();
        // New
        System.out.println("Job is in NEW state.");
        
        IUser user = modelFactory.createUser();
        user.setUsername("user1");
        job.setUser(user);
        job.setEnvironment(modelFactory.createEnvironment());
        job.setExecution(modelFactory.createExecution());

        System.out.println("Persisting job...");
        tx.begin();
        em.persist(user);
        em.persist(job);
        tx.commit();
        // Managed
        System.out.println("Job is in MANAGED state.");

        System.out.println("Removing job...");
        tx.begin();
        em.remove(job);
        // Removed
        System.out.println("Job is in REMOVED state.");
        
        System.out.println("Committing open transaction...");
        tx.commit();
        // Detached
        System.out.println("Job is in DETACHED state.");
    }
}
