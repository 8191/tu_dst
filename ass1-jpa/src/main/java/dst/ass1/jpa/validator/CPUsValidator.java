package dst.ass1.jpa.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * BeanValidator to guarantee an integer value between a specific interval.
 *
 * @author Manuel Faux
 */
public class CPUsValidator implements ConstraintValidator<CPUs, Integer> {

    private Integer min;
    private Integer max;

    @Override
    public void initialize(CPUs a) {
        min = a.min();
        max = a.max();
    }

    @Override
    public boolean isValid(Integer t, ConstraintValidatorContext cvc) {
        if (t == null) {
            return false;
        }

        return t >= min && t <= max;
    }
}
