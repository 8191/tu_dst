package dst.ass1.jpa.validator;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * BeanValidator annotation to guarantee an integer value between a specific
 * interval.
 *
 * @author Manuel Faux
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = CPUsValidator.class)
@Documented
public @interface CPUs {

    public String message() default "{dst.ass1.jpa.validator.CPUs.message}";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};

    public int min();

    public int max();
}
