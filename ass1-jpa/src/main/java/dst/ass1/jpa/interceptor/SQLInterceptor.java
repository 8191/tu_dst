package dst.ass1.jpa.interceptor;

import java.util.concurrent.atomic.AtomicInteger;
import org.hibernate.EmptyInterceptor;

/**
 * JPA interceptor which hooks into JPA backend to audit specific queries.
 *
 * <p>
 * Interceptor needs to be referenced in the persistence configuration.
 * </p>
 * 
 * @author DST LVA Team
 * @author Manuel Faux
 */
public class SQLInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = 3894614218727237142L;
    /**
     * Number of audited select statements.
     */
    private static AtomicInteger audited_selects = new AtomicInteger(0);

    /**
     * Counts the select statements which query a Computer and/or an Execution
     * entity.
     *
     * @param sql the SQL query passed by the JPA backend
     * @return the unchanged query passed by the <code>sql</code> parameter
     */
    @Override
    public String onPrepareStatement(String sql) {
        String sqllow = sql.toLowerCase();
        boolean audit = false;

        // Only select statements are relevant
        if (sqllow.startsWith("select")) {
            // Look for selects on computer and/or execution tables
            String[] tables = sqllow.substring(sqllow.indexOf("from")).split("(from|join)\\s*");
            for (int i = 1; i < tables.length; i++) {
                String table = tables[i].substring(0, tables[i].concat(" ").indexOf(' '));
                if (table.equals("computer") || table.equals("execution")) {
                    audit = true;
                }
            }
        }

        if (audit) {
            audited_selects.getAndIncrement();
            System.out.println("AUDITED SQL: " + sql);
        }
        
        System.out.println("SQL: " + sql);

        return sql;
    }

    /**
     * Resets the audit counter to zero.
     */
    public static void resetCounter() {
        audited_selects.set(0);
    }

    /**
     * Returns the number of audited statements.
     *
     * @return the number of audited statements
     */
    public static int getSelectCount() {
        return audited_selects.get();
    }
}
