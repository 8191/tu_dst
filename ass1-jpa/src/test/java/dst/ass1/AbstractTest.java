package dst.ass1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.After;
import org.junit.Before;

import dst.ass1.jpa.model.ModelFactory;
import dst.ass1.jpa.util.JdbcConnection;
import dst.ass1.jpa.util.JdbcHelper;
import java.sql.SQLException;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public abstract class AbstractTest {

    protected EntityManagerFactory emf;
    protected EntityManager em;
    protected JdbcConnection jdbcConnection;
    protected ModelFactory modelFactory;
    private static boolean setup = false;

    // Added to allow single test execution
    @BeforeClass
    public static void setUpEmf() {
        if (AbstractTestSuite.getEmf() == null) {
            setup = true;
            AbstractTestSuite.setUpClass();
        }
    }
    
    // Added for correct emf close in single test execution scenario
    @AfterClass
    public static void tearDownEmf() throws SQLException {
        if (setup) {
            AbstractTestSuite.tearDownClass();
        }
    }

    @Before
    public void init() throws Exception {
        emf = AbstractTestSuite.getEmf();
        em = emf.createEntityManager();
        jdbcConnection = new JdbcConnection();
        modelFactory = new ModelFactory();

        setUpDatabase();
    }

    @After
    public void clean() throws Exception {
        em.close();
        JdbcHelper.cleanTables(jdbcConnection);
        jdbcConnection.disconnect();
    }

    protected void setUpDatabase() throws Exception {
    }
}
