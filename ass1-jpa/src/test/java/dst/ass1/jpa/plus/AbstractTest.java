/*
 */
package dst.ass1.jpa.plus;

import dst.ass1.AbstractTestSuite;
import dst.ass1.jpa.model.ModelFactory;
import dst.ass1.jpa.util.JdbcConnection;
import dst.ass1.jpa.util.JdbcHelper;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author Manuel Faux
 */
public abstract class AbstractTest {

    protected EntityManagerFactory emf;
    
    protected EntityManager em;
    
    protected JdbcConnection jdbcConnection;
    
    protected ModelFactory modelFactory;
    
    @Before
    public void init() throws Exception {
        if (AbstractTestSuite.getEmf() == null) {
            AbstractTestSuite.setUpClass();
        }

        emf = AbstractTestSuite.getEmf();
        em = emf.createEntityManager();
        jdbcConnection = new JdbcConnection();
        modelFactory = new ModelFactory();

        setUpDatabase();
    }

    @After
    public void clean() throws Exception {
        em.close();
        JdbcHelper.cleanTables(jdbcConnection);
        jdbcConnection.disconnect();

        AbstractTestSuite.tearDownClass();
    }

    protected void setUpDatabase() throws Exception {
    }
}
