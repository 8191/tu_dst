/*
 */
package dst.ass1.jpa.plus;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.dao.*;
import dst.ass1.jpa.model.*;
import dst.ass1.jpa.model.impl.*;
import java.util.List;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import org.hibernate.Session;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel Faux
 */
public class TestBackrel extends AbstractTest {

    @Test
    public void testCreation() {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();

            Admin a1 = new Admin();
            a1.setFirstName("Test2");

            Cluster c = new Cluster();
            c.setName("CL1");
            a1.addCluster(c);
            
            c = new Cluster();
            c.setName("CL2");
            a1.addCluster(c);

            em.persist(a1);

            tx.commit();
            
            DAOFactory daoFactory = new DAOFactory((Session) em.getDelegate());
            IAdminDAO adminDao = daoFactory.getAdminDAO();
            IAdmin admin = adminDao.findById(a1.getId());
            List<ICluster> clusters = admin.getClusters();
            assertEquals(2, clusters.size());
            
        } catch (PersistenceException e) {
            tx.rollback();
            fail(e.getMessage());
        }
    }
}