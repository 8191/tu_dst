package dst.ass1.jpa.plus;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.TestData;
import dst.ass1.jpa.model.IComputer;
import dst.ass1.jpa.model.IGrid;
import dst.ass1.jpa.model.IJob;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.JobStatus;
import dst.ass1.jpa.model.impl.Grid;
import dst.ass1.jpa.model.impl.Job;
import dst.ass1.jpa.model.impl.Membership;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityTransaction;
import static junit.framework.Assert.assertEquals;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel Faux
 */
public class TestGetBill extends AbstractTest {
    
	private TestData testData;

	@Before
	public void setUp() throws NoSuchAlgorithmException {
		testData = new TestData(em);
		testData.insertTestData();
	}
        
        private List<Integer> test112() {
            System.out.println("#@##############");
            List<Integer> a = new ArrayList<Integer>();
            a.add(1);
            a.add(2);
            a.add(3);
            a.add(4);
            return a;
        }

	@SuppressWarnings("unchecked")
	@Test
	public void testFindFreeComputers() {
            for (Integer a : test112()) {
                System.out.println(a);
            }
            
            em.clear();
		EntityTransaction tx = em.getTransaction();
                Session session = (Session)em.getDelegate();
                
		try {
			tx.begin();
                        
                        Criteria jobs = session.createCriteria(Job.class)
                                .createAlias("user", "user")
                                .createAlias("execution", "execution")
                                .add(Restrictions.eq("user.username", "user1"))
                                .add(Restrictions.eq("execution.status", JobStatus.FINISHED))
                                .add(Restrictions.eq("isPaid", false))
                                .setFetchMode("execution.computers.cluster.grid", FetchMode.JOIN);
                        
                        Query memberships = session.createQuery("select m from Membership m join m.id.user u join fetch m.id.grid where u.username = :username");
                        memberships.setString("username", "user2");
                        
                        List<IJob> result = jobs.list();
                        //List<IMembership> mem = memberships.list();
//                        System.out.println("Finished fetching.........");
//                        for (IMembership m : result.get(0).getExecution().getComputers().get(0).getCluster().getGrid().getMemberships()) {
//                            System.out.print(m.getId().getUser().getUsername());
//                            System.out.println(m.getDiscount());
//                        }
//                        System.out.println(result.get(0).getExecution().getComputers().get(0).getCluster().getGrid().getName());
                        
                        //System.out.println(String.format("%d computers found:", result.size()));
                        for (IJob r : result)
                            System.out.println(r.getExecution().getComputers().get(0).getCluster().getGrid().getName());
                        
			//assertNotNull(result);
			//assertEquals(3, result.size());
		} catch (Exception e) {
			fail(e.getMessage());
		} finally {
			tx.rollback();
		}
	}
}