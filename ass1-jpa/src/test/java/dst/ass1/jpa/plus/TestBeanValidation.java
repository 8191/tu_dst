package dst.ass1.jpa.plus;

import dst.ass1.jpa.*;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Test;

import dst.ass1.jpa.model.IComputer;
import dst.ass1.jpa.model.ModelFactory;

public class TestBeanValidation {
	
	private ModelFactory modelFactory = new ModelFactory();

	@Test
	public void testBuiltInConstaintsInvalid1() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator val = vf.getValidator();

		IComputer computer = modelFactory.createComputer();
		computer.setName("comput");
		computer.setCpus(5);
		computer.setCreation(createDate(2019, 01, 01));
		computer.setLastUpdate(createDate(2019, 01, 01));

		Set<ConstraintViolation<IComputer>> violation = val.validate(computer);
		assertNotNull(violation);
		assertEquals(2, violation.size());	
	}
    
	@Test
	public void testBuiltInConstaintsInvalid2() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator val = vf.getValidator();

		IComputer computer = modelFactory.createComputer();
		computer.setName("comput");
		computer.setCpus(5);
		computer.setLocation("AUT-VE@333");
        
		Set<ConstraintViolation<IComputer>> violation = val.validate(computer);
		assertNotNull(violation);
		assertEquals(1, violation.size());	
	}
    
	@Test
	public void testBuiltInConstaintsInvalid3() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator val = vf.getValidator();

		IComputer computer = modelFactory.createComputer();
		computer.setName("comt");
		computer.setCpus(5);

		Set<ConstraintViolation<IComputer>> violation = val.validate(computer);
		assertNotNull(violation);
		assertEquals(1, violation.size());	
	}
        
    
	@Test
	public void testBuiltInConstaintsInvalid4() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator val = vf.getValidator();

		IComputer computer = modelFactory.createComputer();
		computer.setName("comput");
		computer.setCpus(3);

		Set<ConstraintViolation<IComputer>> violation = val.validate(computer);
		assertNotNull(violation);
		assertEquals(1, violation.size());	
	}
        
	@Test
	public void testBuiltInConstaintsInvalid5() {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator val = vf.getValidator();

		IComputer computer = modelFactory.createComputer();
		computer.setName("comput");
		computer.setCpus(9);

		Set<ConstraintViolation<IComputer>> violation = val.validate(computer);
		assertNotNull(violation);
		assertEquals(1, violation.size());	
	}

	
	private Date createDate(int year, int month, int day) {

		String temp = year + "/" + month + "/" + day;
		Date date = null;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			date = formatter.parse(temp);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return date;

	}	
}
