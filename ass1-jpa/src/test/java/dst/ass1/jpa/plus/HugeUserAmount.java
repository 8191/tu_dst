package dst.ass1.jpa.plus;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.dao.*;
import dst.ass1.jpa.model.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel
 */
public class HugeUserAmount extends AbstractTest {

    private MessageDigest md;
    private IUserDAO userDao;
    private DAOFactory daoFactory;
    private List<IUser> users;
    private Random rand;
    private BufferedWriter log;

    @Before
    public void prepare() throws NoSuchAlgorithmException, IOException {
        daoFactory = new DAOFactory((Session) em.getDelegate());
        userDao = daoFactory.getUserDAO();
        md = MessageDigest.getInstance("MD5");
        rand = new Random();
        users = new ArrayList<IUser>();
        log = new BufferedWriter(new FileWriter("perf.log"));
    }
    
    @After
    public void close() throws IOException {
        log.close();
    }

    private void setUpDb(final int rows) throws NoSuchAlgorithmException {
        int usercount = userDao.findAll().size();
        if (usercount < rows) {
            EntityTransaction tx = em.getTransaction();

            try {
                tx.begin();
                for (Integer i = 0; i < rows - usercount; i++) {
                    IUser u = modelFactory.createUser();
                    users.add(u);
                    u.setUsername("User " + i.toString() + " - " + rand.nextInt());
                    u.setPassword(md.digest(i.toString().getBytes()));
                    em.persist(u);
                }
                tx.commit();
            } catch (PersistenceException e) {
                tx.rollback();
                fail(e.getMessage());
            }
        }

        assertEquals(rows, users.size());
    }

    private long testSelect(final int rows, final int selects) throws IOException {
        try {
            setUpDb(rows);
        } catch (NoSuchAlgorithmException e) {
            fail(e.getMessage());
        }

        long startTime = System.nanoTime();
        for (Integer i = 0; i < selects; i++) {
            Integer a = (rows / selects) * i;
            userDao.findById(users.get(a).getId());
        }
        long endTime = System.nanoTime();
        long time = endTime - startTime;

        System.out.format("%d, %d: %d\n", rows, selects, time);
        log.write(String.format("%d, %d: %d\r\n", rows, selects, time));

        return time;
    }

    @Test
    public void testSelect1() throws IOException {
        testSelect(100, 10);
        testSelect(100, 10);
        testSelect(1000, 10);
        testSelect(1000, 10);
        testSelect(10000, 10);
        testSelect(10000, 10);
        testSelect(100000, 10);
        testSelect(100000, 10);
        testSelect(100000, 100);
        testSelect(100000, 100);
    }
}