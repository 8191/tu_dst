package dst.ass1.jpa.plus;

import dst.ass1.jpa.Test_2a01_1;
import dst.ass1.jpa.Test_2a01_2;
import dst.ass1.jpa.Test_2a02_1;
import dst.ass1.jpa.Test_2a02_2;
import dst.ass1.jpa.Test_2a02_3;
import dst.ass1.jpa.Test_2a02_4;
import dst.ass1.jpa.Test_2b01_1;
import dst.ass1.jpa.Test_2b01_2;
import dst.ass1.jpa.Test_2c01;
import dst.ass1.jpa.Test_2c02;
import dst.ass1.jpa.Test_2d;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Manuel Faux
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    Test_2a01_1.class, Test_2a01_2.class, Test_2a02_1.class, Test_2a02_2.class,
    Test_2a02_3.class, Test_2a02_4.class, Test_2b01_1.class, Test_2b01_2.class,
    Test_2c01.class, Test_2c02.class, Test_2d.class
})
public class Ass1JPA_2 {
}