package dst.ass1.jpa.plus;

import dst.ass1.jpa.interceptor.SQLInterceptor;
import javax.persistence.Query;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel
 */
public class TestInterceptor extends dst.ass1.AbstractTest {
    
    private void parseTables(String sql) {
        System.out.println(sql);
        String[] tables = sql.substring(sql.indexOf("from")).split("(from|join)\\s*");
        for (int i = 1; i < tables.length; i++) {
            String string = tables[i];
            System.out.println(">"+string.substring(0,string.concat(" ").indexOf(' ') )+"<");
        }
        
        System.out.println();
    }
    
    @Test
    public void testParse() {
        parseTables("select u.name, r.name from user join grid on u.grid = r.id");
        parseTables("select u.name, r.name from user u join grid r on u.grid = r.id");
        parseTables("select u.name, r.name from user");
        parseTables("select u.name, r.name from user u");
        parseTables("select * from user");
    }
    
    @Test
    public void testInterceptor() {
        SQLInterceptor.resetCounter();

        Query c = em.createQuery("select c from Computer c");
        c.getResultList();

        assertEquals(1, SQLInterceptor.getSelectCount());

        c = em.createQuery("select distinct c from User c");
        c.getResultList();

        assertEquals(1, SQLInterceptor.getSelectCount());

        c = em.createQuery("select e from Execution e");
        c.getResultList();
        assertEquals(2, SQLInterceptor.getSelectCount());
    }
}