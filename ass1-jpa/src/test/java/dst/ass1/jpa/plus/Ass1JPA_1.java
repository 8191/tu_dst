package dst.ass1.jpa.plus;

import dst.ass1.AbstractTestSuite;
import dst.ass1.jpa.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Manuel Faux
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    Test_1a01.class, Test_1a02.class, Test_1a03.class,
    Test_1a04.class, Test_1a06.class, Test_1a07.class, Test_1a08.class,
    Test_1a09.class, Test_1a10.class, Test_1a11.class, Test_1a12.class,
    Test_1a13.class, Test_1a14.class, Test_1a15.class, Test_1a16.class,
    Test_1a17.class, Test_1a18.class,
    Test_1b01.class, Test_1b02.class
})
public class Ass1JPA_1 extends AbstractTestSuite {
}