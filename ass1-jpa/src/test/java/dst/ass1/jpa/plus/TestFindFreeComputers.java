package dst.ass1.jpa.plus;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.TestData;
import dst.ass1.jpa.model.IComputer;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import static junit.framework.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel Faux
 */
public class TestFindFreeComputers extends AbstractTest {
    
	private TestData testData;

	@Before
	public void setUp() throws NoSuchAlgorithmException {
		testData = new TestData(em);
		testData.insertTestData();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testFindFreeComputers() {
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			Query query = em.createNamedQuery("findFreeComputers");
                        //query.setParameter("grid_id", testData.getGridId());

			List<IComputer> result = (List<IComputer>) query.getResultList();
                        
                        System.out.println(String.format("%d computers found:", result.size()));
                        for (IComputer computer : result)
                            System.out.println(computer);
                        
			assertNotNull(result);
			assertEquals(3, result.size());
		} catch (Exception e) {
			fail(e.getMessage());
		} finally {
			tx.rollback();
		}
	}
}