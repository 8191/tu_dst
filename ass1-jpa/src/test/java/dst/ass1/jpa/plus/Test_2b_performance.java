package dst.ass1.jpa.plus;

import dst.ass1.jpa.*;
import static junit.framework.Assert.assertEquals;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.interceptor.SQLInterceptor;
import dst.ass1.jpa.model.IComputer;

public class Test_2b_performance extends AbstractTest {

    private TestData testData;

    @Before
    public void setUp() throws NoSuchAlgorithmException {
        testData = new TestData(em);
        testData.insertTestData();
    }

    @Test
    public void testFindComputersInVienna() {
        SQLInterceptor.resetCounter();
        
        // Create new entity manager to prevent cache reusing
        em.close();
        em = emf.createEntityManager();
        
        DAOFactory daoFactory = new DAOFactory((Session) em.getDelegate());
        HashMap<IComputer, Integer> usageMap = daoFactory.getComputerDAO()
                        .findComputersInViennaUsage();
        assertEquals(3, usageMap.size());

        System.out.println("SELECT COUNT: " + SQLInterceptor.getSelectCount());
    }
}
