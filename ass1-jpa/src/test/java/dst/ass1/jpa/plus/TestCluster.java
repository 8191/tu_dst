package dst.ass1.jpa.plus;

import dst.ass1.jpa.model.ICluster;
import dst.ass1.jpa.model.ModelFactory;
import dst.ass1.jpa.util.JdbcConnection;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel Faux
 */
public class TestCluster {

    protected EntityManagerFactory emf;
    protected EntityManager em;
    protected JdbcConnection jdbcConnection;
    protected ModelFactory modelFactory;

    public TestCluster() {
    }

    @Before
    public void init() {
        emf = Persistence.createEntityManagerFactory("dst_pu");
        em = emf.createEntityManager();
        jdbcConnection = new JdbcConnection();
        modelFactory = new ModelFactory();
    }

    @Test
    public void createCluster() {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();

            // Clusters
            ICluster cluster1 = modelFactory.createCluster();
            //cluster1.setAdmin(admin1);
            cluster1.setName("cluster1");
            cluster1.setLastService(new Date());
            cluster1.setNextService(new Date());

            em.persist(cluster1);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            fail(e.getMessage());
        }
    }

    protected void setUpDatabase() {
    }
}