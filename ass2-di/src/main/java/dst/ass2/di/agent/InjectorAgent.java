package dst.ass2.di.agent;

import dst.ass2.di.annotation.Component;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.NotFoundException;

public class InjectorAgent implements ClassFileTransformer {

    private ClassPool classPool = ClassPool.getDefault();
    
    @Override
    public byte[] transform(ClassLoader loader, String className,
            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
            byte[] classfileBuffer) throws IllegalClassFormatException {
        try {
            CtClass cls = classPool.makeClass(new ByteArrayInputStream(classfileBuffer));
            if (cls.hasAnnotation(Component.class)) {
                // Add code to default constructor to invoke InjectionController
                CtConstructor constructor = cls.getConstructor("()V");
                constructor.insertAfter("dst.ass2.di.InjectionControllerFactory.getStandAloneInstance().initialize(this);");
                
                // Generate parameterized constructor to be called from InjectorController
                CtClass[] params = new CtClass[] { classPool.get("dst.ass2.di.annotation.Component") };
                constructor = new CtConstructor(params, cls);
                constructor.setBody("return;");
                cls.addConstructor(constructor);
                
                return cls.toBytecode();
            }
        } catch (IOException ex) {
            Logger.getLogger(InjectorAgent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RuntimeException ex) {
            Logger.getLogger(InjectorAgent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotFoundException ex) {
            Logger.getLogger(InjectorAgent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CannotCompileException ex) {
            Logger.getLogger(InjectorAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return classfileBuffer;
    }
    
    public static void premain(String agentArgs, Instrumentation inst) {
        inst.addTransformer(new InjectorAgent());
    }
}
