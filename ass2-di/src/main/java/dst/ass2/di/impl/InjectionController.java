package dst.ass2.di.impl;

import dst.ass2.di.IInjectionController;
import dst.ass2.di.InjectionException;
import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;
import dst.ass2.di.model.ScopeType;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Manuel
 */
public class InjectionController implements IInjectionController {
    
    private Map<Class<?>, Object> singletons;
    
    private AtomicLong componentIdGenerator;
    
    public InjectionController() {
        singletons = Collections.synchronizedMap(new HashMap<Class<?>, Object>());
        componentIdGenerator = new AtomicLong();
    }

    @Override
    public synchronized void initialize(Object obj) throws InjectionException {
        Component component = obj.getClass().getAnnotation(Component.class);
        if (component == null) {
            throw new InjectionException(obj.getClass() + " is not annotated with Component.");
        }
        
        if (component.scope().equals(ScopeType.SINGLETON)) {
            if (singletons.containsKey(obj.getClass())) {
                throw new InjectionException("Singleton " + obj.getClass() + " has already been initialized.");
            }
            singletons.put(obj.getClass(), obj);
        }
        
        initializeFields(obj, obj.getClass(), null);
    }
    
    private void initializeFields(Object obj, Class<?> cls, Long id) {
        Field componentId = null;
        for (Field field : cls.getDeclaredFields()) {
            ComponentId idAnnotation = field.getAnnotation(ComponentId.class);
            Inject inject = field.getAnnotation(Inject.class);
            
            if (idAnnotation != null) {
                if (componentId != null)
                    throw new InjectionException(cls + " has more than one ComponentId.");
                else if (inject != null)
                    throw new InjectionException("ComponentId of " + cls + " must not be annotated with Inject.");
                else if (field.getType().equals(Long.TYPE.getClass()))
                    throw new InjectionException("ComponentId of " + cls + " needs to be a Long value but is " + field.getType());
                
                componentId = field;
            }
            
            if (inject != null) {
                try {
                    initializeField(field, obj, obj.getClass());
                } catch (InjectionException ex) {
                    if (inject.required() == true) 
                        throw ex;
                } catch (IllegalArgumentException ex) {
                    if (inject.required() == true)
                        throw ex;
                } catch (IllegalAccessException ex) {
                    throw new InjectionException("Could not access and instanciate field " + field.getName() + " of "
                            + cls + ": " + ex.getMessage());
                }
            }
        }
        
        if (componentId == null)
            throw new InjectionException(cls + " does not have a ComponentId.");
        
        if (id == null)
            id = componentIdGenerator.getAndIncrement();
        
        if (cls.getSuperclass().getAnnotation(Component.class) != null)
            initializeFields(obj, cls.getSuperclass(), id);
            
        try {
            componentId.setAccessible(true);
            componentId.set(obj, id);
            System.out.println("Setting ID " + id + " for " + obj + " (" + cls + ")");
        } catch (IllegalAccessException ex) {
            throw new InjectionException("Could not set ComponentId for " + cls + ".");
        }
    }
    
    private void initializeField(Field field, Object obj, Class cls) throws IllegalAccessException {
        Inject inject = field.getAnnotation(Inject.class);
        Class<?> newType = inject.specificType() == Void.class ? field.getType() : inject.specificType();
        Component component = newType.getAnnotation(Component.class);
        if (component == null) {
                throw new InjectionException(cls + " specified " + field.getName() + " as "
                        + newType + " which is not annotated with Component.");
        }
        
        Object ref = singletons.get(newType);
        if (ref == null) {
            try {
                try {
                    ref = newType.getDeclaredConstructor(Component.class).newInstance(component);
                } catch (NoSuchMethodException ex) {
                    ref = newType.newInstance();
                } catch (InvocationTargetException ex) {
                    ref = newType.newInstance();
                }
            } catch (InstantiationException ex) {
                throw new InjectionException("Could not instantiate " + newType + " in " + cls + ": " + ex.getMessage());
            }
            
            if (component.scope().equals(ScopeType.SINGLETON)) {
                singletons.put(newType, ref);
            }
            
            initializeFields(ref, ref.getClass(), null);
        }
        
        field.setAccessible(true);
        field.set(obj, ref);
    }

    @Override
    public synchronized <T> T getSingletonInstance(Class<T> cls) throws InjectionException {
        Component component = cls.getAnnotation(Component.class);
        if (component == null)
            throw new InjectionException(cls + " is not annotated with Component.");
        else if (!component.scope().equals(ScopeType.SINGLETON))
            throw new InjectionException(cls + " is not a singleton.");
        
        T ref = (T) singletons.get(cls);
        if (ref == null) {
            try {
                ref = cls.newInstance();
            } catch (InstantiationException ex) {
                throw new InjectionException("Could not instantiate " + cls + ": " + ex.getMessage());
            } catch (IllegalAccessException ex) {
                throw new InjectionException("Could not access and instanciate " + cls + ".");
            }
            initialize(ref);
        }
        
        return ref;
    }
    
}
