package dst.ass3.aop.plus;

import org.aspectj.lang.annotation.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

/**
 *
 * @author Manuel Faux
 */
public class AJTest {

    public AJTest() {
    }
    
    @Test
    public void abcTest() {
        AspectJProxyFactory factory = new AspectJProxyFactory(new Test1());
        factory.addAspect(new Aspect1());
        Test1 test1 = factory.<Test1>getProxy();
        test1.abc();
        
        factory = new AspectJProxyFactory(new Test2());
        factory.addAspect(new Aspect1());
        Test2 test2 = factory.<Test2>getProxy();
        test2.abc();
    }
    
}
