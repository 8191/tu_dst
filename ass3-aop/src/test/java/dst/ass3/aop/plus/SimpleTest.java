package dst.ass3.aop.plus;

import dst.ass3.aop.IPluginExecutable;
import dst.ass3.aop.IPluginExecutor;
import dst.ass3.aop.PluginExecutorFactory;
import dst.ass3.aop.impl.ThreadPool;
import static dst.ass3.aop.util.PluginUtils.ALL_FILE;
import static dst.ass3.aop.util.PluginUtils.SIMPLE_FILE;
import static dst.ass3.aop.util.PluginUtils.preparePlugin;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author Manuel
 */
public class SimpleTest {

    public static final File PLUGINS_DIR = new File(FileUtils.getTempDirectoryPath(), "plugins_" + System.currentTimeMillis());
    public static File SIMPLE_FILE;

    static {
        try {
            ALL_FILE = new ClassPathResource("all.zip").getFile();
            SIMPLE_FILE = new ClassPathResource("simple.zip").getFile();
        } catch (IOException e) {
            throw new RuntimeException("Cannot locate plugin in classpath", e);
        }
    }

    public SimpleTest() {
    }

    @Test
    public void test1() throws IOException, InterruptedException {
        //preparePlugin(SIMPLE_FILE);

        IPluginExecutor exec = PluginExecutorFactory.createPluginExecutor();
        exec.monitor(new File("C:\\Users\\Manuel\\AppData\\Local\\Temp\\test1"));
        exec.monitor(new File("C:\\Users\\Manuel\\AppData\\Local\\Temp\\test2"));
        exec.start();
        Thread.sleep(100000);
    }
    
    @Test
    public void test2() throws InterruptedException {
        ThreadPool threadPool = new ThreadPool(1);
        threadPool.start();
        Thread.sleep(1000);
        threadPool.execute(new Runnable() {

            @Override
            public void run() {
                System.out.println("Task running...");
            }
        });
    }
}