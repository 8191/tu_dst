/*
 */
package dst.ass3.aop.plus;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 *
 * @author Manuel Faux
 */
@Aspect
public class Aspect1 {

    @Pointcut("execution(void Test1.abc())")
    public void abcCall() {}
    
    @After("abcCall()")
    public void beforeAbcCall() {
        System.out.println("Abc call**********.");
    }
}
