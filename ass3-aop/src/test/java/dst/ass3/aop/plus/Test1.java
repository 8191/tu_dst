/*
 */
package dst.ass3.aop.plus;

import org.aspectj.lang.annotation.Pointcut;

/**
 *
 * @author Manuel Faux
 */
public class Test1 {

    public void abc() {
        System.out.println("Abc run.");
    }
}
