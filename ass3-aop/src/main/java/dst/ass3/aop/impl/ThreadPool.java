package dst.ass3.aop.impl;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Manuel Faux
 * @see http://www.ibm.com/developerworks/library/j-jtp0730/index.html
 */
public class ThreadPool {
    
    private static final Logger log = Logger.getLogger(PluginExecutor.class.getName());

    private final Worker[] workers;
    
    private final LinkedList<Runnable> queue;
    
    private volatile boolean stopped = true;
    
    public ThreadPool(int threads) {
        queue = new LinkedList<Runnable>();
        workers = new Worker[threads];
        
        for (int i = 0; i < threads; i++) {
            workers[i] = new Worker();
        }
    }
    
    public void start() {
        synchronized (queue) {
            stopped = false;
            
            for (int i = 0; i < workers.length; i++) {
                workers[i].start();
            }
        }
    }
    
    public void stop() {
        synchronized (queue) {
            stopped = true;
            queue.notifyAll();
        }
    }
    
    public void execute(Runnable r) {
        synchronized (queue) {
            queue.addLast(r);
            queue.notify();
        }
    }
    
    private class Worker extends Thread {

        @Override
        public void run() {
            Runnable r;
            
            while (true) {
                synchronized (queue) {
                    while (!stopped && queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException ex) {
                        }
                    }
                    
                    if (stopped) {
                        return;
                    }

                    r = queue.removeFirst();
                }

                try {
                    r.run();
                }
                catch (RuntimeException ex) {
                    log.log(Level.SEVERE, "Exception in executing plugin", ex);
                }
            }
        }
    }
}
