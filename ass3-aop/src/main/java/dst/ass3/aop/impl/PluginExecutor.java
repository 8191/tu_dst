package dst.ass3.aop.impl;

import dst.ass3.aop.IPluginExecutable;
import dst.ass3.aop.IPluginExecutor;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent;

import java.util.HashMap;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * @author Manuel Faux
 */
public class PluginExecutor implements IPluginExecutor {

    private static final Logger log = Logger.getLogger(PluginExecutor.class.getName());
    
    private final WatchService watchService;
    
    private final ThreadPool threadPool;
    
    private final HashMap<File, WatchKey> watches;
    
    private final DirWatcher watcher;
    
    private volatile boolean running = false;
    
    public PluginExecutor() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        threadPool = new ThreadPool(5);
        watches = new HashMap<File, WatchKey>();
        watcher = new DirWatcher();
    }
    
    @Override
    public void monitor(File dir) {
        try {
            synchronized (watchService) {
                WatchKey key = dir.toPath().register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
                watches.put(dir, key);
            }
            log.log(Level.INFO, "Monitoring directory {0}", dir.getAbsolutePath());
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Cannot monitor directory {0}", dir.getAbsolutePath());
        }
    }

    @Override
    public void stopMonitoring(File dir) {
        synchronized (watchService) {
            if (watches.get(dir) != null) {
                watches.get(dir).cancel();
                watches.remove(dir);
            }
        }
    }

    @Override
    public synchronized void start() {
        if (running) {
            return;
        }
        
        running = true;
        threadPool.start();
        watcher.start();
    }

    @Override
    public void stop() {
        synchronized (this) {
            if (!running) {
                return;
            }

            running = false;
            watcher.interrupt();
            threadPool.stop();
            
            // Stop watcher
            synchronized (watchService) {
                watchService.notifyAll();
            }
        }
    }
    
    private void loadPlugins(File file) {
        try {
            JarFile jar = new JarFile(file);
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();

                ClassLoader cl = new URLClassLoader(new URL[]{file.toURI().toURL()}, PluginExecutor.class.getClassLoader());
                try {
                    Class<?> cls = Class.forName(entry.getName().replaceAll(".class", "").replaceAll("/", "."), true, cl);

                    if (!implementsInterface(cls, IPluginExecutable.class)) {
                        log.log(Level.FINER, "Ommiting not executable class {0}", cls.getCanonicalName());
                        continue;
                    }

                    log.log(Level.FINE, "Loaded class {0}", entry.getName());
                    startPlugin((Class<IPluginExecutable>) cls);
                } catch (ClassNotFoundException ex) {
                        log.log(Level.WARNING, "Could not load class file {0}", entry.getName());
                }
            }
            
            jar.close();
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Could not load file " + file, ex);
        }
    }
    
    private boolean implementsInterface(Class<?> cls, Class<?> iface) {
        for (Class i : cls.getInterfaces()) {
            if (i.equals(iface)) {
                return true;
            }
        }
        
        return false;
    }
    
    private void startPlugin(final Class<IPluginExecutable> cls) {
        Runnable pluginThread = new Runnable() {

            @Override
            public void run() {
                try {
                    Constructor<IPluginExecutable> constr = cls.getConstructor();
                    IPluginExecutable plugin = (IPluginExecutable)constr.newInstance();
                    plugin.execute();
                } catch (Exception ex) {
                    log.log(Level.SEVERE, "Failed to execute plugin " + cls.getCanonicalName(), ex);
                }
            }
        };
        log.log(Level.INFO, "Executing plugin {0}", cls.getCanonicalName());
        threadPool.execute(pluginThread);
    }
    
    private class DirWatcher extends Thread {

        @Override
        public void run() {
            while (true) {
                WatchKey key;
                
                try {
                    key = watchService.take();
                }
                catch (InterruptedException ex){
                    return;
                }
                
                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();
                    
                    if (kind == OVERFLOW) {
                        continue;
                    }
                    
                    WatchEvent<Path> ev = (WatchEvent<Path>)event;
                    Path filename = ev.context();
                    
                    if (!filename.toString().contains(".jar")) {
                        continue;
                    }
                    
                    Path dir = (Path)key.watchable();
                    Path fullpath = dir.resolve(filename);
                    log.log(Level.FINE, "File change {0} detected", fullpath.toString());
                    loadPlugins(fullpath.toFile());
                }
                
                key.reset();
            }
        }
        
    }
}
