package dst.ass3.aop.management;

import dst.ass3.aop.IPluginExecutable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.aspectj.lang.annotation.*;

@Aspect
public class ManagementAspect {
	
    private final HashMap<IPluginExecutable, Long> timelimits = new HashMap<IPluginExecutable, Long>();
    
    private final TimeoutEnforcement timeoutEnforcement = new TimeoutEnforcement();

    public ManagementAspect() {
        timeoutEnforcement.start();
    }
    
	@Pointcut("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && @annotation(Timeout)")
	public void execute(IPluginExecutable executable) {}
	    
    @Before("execute(plugin)")
    public void beforeExecute(IPluginExecutable plugin) {
        Timeout timeout;
        try {
            timeout = plugin.getClass().getMethod("execute", (Class<?>[]) null).getAnnotation(Timeout.class);
        } catch (NoSuchMethodException ex) {
            return;
        }
        
        synchronized (timelimits) {
            System.out.println("queue");
            timelimits.put(plugin, System.currentTimeMillis() + timeout.value());
            timelimits.notify();
        }
    }
    
    @After("execute(plugin)")
    public void afterExecute(IPluginExecutable plugin) {
        synchronized (timelimits) {
            timelimits.remove(plugin);
        }
    }

    private class TimeoutEnforcement extends Thread {

        @Override
        public void run() {
            while (true) {
                synchronized (timelimits) {
                    while (timelimits.isEmpty()) {
                        try {
                            timelimits.wait();
                        } catch (InterruptedException ex) {
                        }
                    }
                    
                    Iterator<Map.Entry<IPluginExecutable, Long>> it = timelimits.entrySet().iterator();
                    long time = System.currentTimeMillis();
                    while (it.hasNext()) {
                        Map.Entry<IPluginExecutable, Long> pairs = it.next();
                        if (pairs.getValue() > time) {
                            pairs.getKey().interrupted();
                        }
                    }
                }
                
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
        }
        
    }
    
}
