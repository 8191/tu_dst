package dst.ass3.aop.logging;

import dst.ass3.aop.IPluginExecutable;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.aspectj.lang.annotation.*;

@Aspect
public class LoggingAspect {
	
    @Pointcut("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && !@annotation(Invisible)")
	public void execute(IPluginExecutable executable) {}
	    
    @Before("execute(plugin)")
    public void beforeExecute(IPluginExecutable plugin) {
        log(plugin, "starting to execute");
    }
    
    @After("execute(plugin)")
    public void afterExecute(IPluginExecutable plugin) {
        log(plugin, "execution finished");
    }
    
    private void log(Object obj, String msg) {
        Logger logger = getLogger(obj);
        
        if (logger != null) {
            logger.logp(Level.INFO, obj.getClass().getCanonicalName(), "call", msg);
            //logger.log(Level.INFO, msg);
        } else {
            System.out.println("Plugin " + obj.getClass().getCanonicalName() + " " + msg);
        }
    }
    
    private Logger getLogger(Object obj) {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.getType().equals(Logger.class)) {
                try {
                    field.setAccessible(true);
                    return (Logger)field.get(obj);
                } catch (IllegalAccessException ex) {
                    return null;
                }
            }
        }
        
        return null;
    }
}
